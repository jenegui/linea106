<?php
/*
 * Modelo para generar los reportes
 */

class Reportes_model extends CI_Model {
    
function __construct()
    {
        parent::__construct();
    }

    //Colsulta general por sexo y grupos de edad
    function general($campo, $tabla)
    {
        $id = $campo."_id";
        $id_llam = $campo."_id_llam";
        
        //Reporte agrupado por sexo y edad
        $this->db->select($campo.' AS concepto');
        $this->db->join($tabla, $id.' = '.$id_llam, 'left');
        $this->db->group_by($campo);
        $this->db->order_by($id);

        $this->db->select('sexo');
        $this->db->select('rango');
        $this->db->select('COUNT(*) as total');
        $this->db->where('hora_inicio >', $this->session->inicio.' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->final.' 23:59:59');
        if ($this->session->tipo_usuario != 'administrador') {
            $this->db->where('profesional', $this->session->usuario);
        }

        $this->db->join('edades', 'edades.edad = llamadas.edad', 'left');
        $this->db->group_by('sexo');
        $this->db->group_by('rango');
        $this->db->order_by('sexo');
        $this->db->order_by('llamadas.edad');
        return $this->db->get('llamadas');
    }

    //Cunsulta por profesional
    function profesional($campo)
    {
        $tabla = $campo."s";
        $id = $campo."_id";
        $id_llam = $campo."_id_llam";

        $this->db->select($campo.' AS concepto');
        $this->db->join($tabla, $id.' = '.$id_llam, 'left');
        $this->db->group_by($id);
        $this->db->order_by($id);
        
        $this->db->select('COUNT(*) as total');
        $this->db->where('hora_inicio >', $this->session->userdata('inicio').' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->userdata('final').' 23:59:59');
        $this->db->where('profesional', $this->session->userdata('usuario'));

        return $this->db->get('llamadas');

    }

    //Colsulta por sexo y grupos de edad para determinada poblacion
    function poblacion($campo)
    {
        //Reporte agrupado por sexo y edad
        $this->db->select('motivo AS concepto');
        $this->db->join('motivos', 'motivo_id = motivo_id_llam');
        $this->db->group_by('motivo');
        $this->db->order_by('motivo_id');

        $this->db->select('sexo');
        $this->db->select('rango');
        $this->db->select('COUNT(*) as total');
        $this->db->where('hora_inicio >', $this->session->userdata('inicio').' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->userdata('final').' 23:59:59');
        $this->db->where($campo, '1');
        if ($this->session->tipo_usuario != 'administrador') {
            $this->db->where('profesional', $this->session->usuario);
        }

        $this->db->join('edades', 'edades.edad = llamadas.edad', 'left');
        $this->db->group_by('sexo');
        $this->db->group_by('rango');
        $this->db->order_by('sexo');
        $this->db->order_by('llamadas.edad');
        return $this->db->get('llamadas');
    }
    
    //Consulat por entidad a la que se remite
    function entidades()
    {
        $this->db->select('entidad AS concepto');
        $this->db->select('COUNT(*) as total');
        $this->db->group_by('entidad');
        $this->db->order_by('total','DESC');
        if ($this->session->tipo_usuario != 'administrador') {
            $this->db->where('profesional', $this->session->usuario);
        }
        $this->db->where('hora_inicio >', $this->session->inicio.' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->final.' 23:59:59');
        $this->db->where('entidad != "" ');
        return $this->db->get('llamadas');
    }

}