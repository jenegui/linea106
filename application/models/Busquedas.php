<?php
/*
 * Modelo con resultados del buscador
 
	=====================================================================================================
	Author Original: Berdejo Casas, Juan Pablo
	Author Modificación: Serna Gonzálaez, Paola
	Fecha Modificacion: 28/12/2016
	Descripcion Modificacion: 
		1. Nuevo campo RESPUESTAS EFECTIVAS (funcion campos)
		
	======================================================================================================

 */

class Busquedas extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    //Total del buscador
    function filas($palabra)
    {
        /*
        $this->db->join('localidades', 'localidad_id = localidad_id_llam', 'left');
        $this->db->like('telefono', $palabra);
        $this->db->or_like('primer_nombre', $palabra);
        $this->db->or_like('segundo_nombre', $palabra);
        $this->db->or_like('primer_apellido', $palabra);
        $this->db->or_like('segundo_apellido', $palabra);
        $this->db->or_like('localidad', $palabra);
        $this->db->or_like('colegio_jardin', $palabra);
        $this->db->or_like('narrativa', $palabra);
        $this->db->or_like('intervencion', $palabra);
        $this->db->or_like('profesional', $palabra);
        */
        $sql = "SELECT COUNT(*) AS total FROM l106__llamadas 
                WHERE MATCH(`primer_nombre` ,
                    `segundo_nombre` ,
                    `primer_apellido` ,
                    `segundo_apellido` ,
                    `narrativa` )
                AGAINST ('$palabra')";
        $query = $this->db->query($sql);
        return $query->first_row()->total;
    }

    //Query con la busqueda
    function llamadas ($palabra, $filas, $desde) 
    {
        /*
        $this->db->join('motivos', 'motivo_id = motivo_id_llam');
        $this->db->join('lineas', 'linea_id = linea_id_llam');
        $this->db->join('localidades', 'localidad_id = localidad_id_llam', 'left');
        $this->db->order_by("llamada_id", "desc");
        
        $this->db->like('telefono', $palabra);
        $this->db->or_like('hora_inicio', $palabra);
        $this->db->or_like('primer_nombre', $palabra);
        $this->db->or_like('segundo_nombre', $palabra);
        $this->db->or_like('primer_apellido', $palabra);
        $this->db->or_like('segundo_apellido', $palabra);
        $this->db->or_like('localidad', $palabra);
        $this->db->or_like('colegio_jardin', $palabra);
        $this->db->or_like('narrativa', $palabra);
        $this->db->or_like('intervencion', $palabra);
        $this->db->or_like('profesional', $palabra);

        return $this->db->get('llamadas', $filas, $desde);
         * 
         */
        $sql = "SELECT * FROM l106__llamadas 
                JOIN l106__motivos ON motivo_id = motivo_id_llam
                JOIN l106__lineas ON linea_id = linea_id_llam
                LEFT JOIN l106__localidades ON localidad_id = localidad_id_llam
                LEFT JOIN l106__violencias ON llamada_id = id_llamada_vio
                LEFT JOIN l106__lugares ON id_lugar = id_lugar_ocurre
                LEFT JOIN l106__relaciones ON id_relacion = id_relacion_agresor_victima
                WHERE MATCH(`primer_nombre` ,
                    `segundo_nombre` ,
                    `primer_apellido` ,
                    `segundo_apellido` ,
                    `narrativa` )
                AGAINST ('$palabra')
                ORDER BY llamada_id DESC
                LIMIT $desde, $filas ";
        return $this->db->query($sql);
    }
    
    function campos()
    {
        //PREPARAR CAMPOS DEL FORMULARIO
        date_default_timezone_set('America/Bogota');
        $data['fecha'] = date("Y-m-d");
        $data['hora'] = date("H:i");

        $query = $this->db->get("tipos_doc");
        foreach ($query->result() as $row) {
            $data['documentos'][$row->tipo_doc_id] = $row->tipo_doc;
        }
        
        $data['tipos_edu'][''] = '-';
        $query = $this->db->get("tipos_edu");
        foreach ($query->result() as $row) {
            $data['tipos_edu'][$row->id_tipo_edu] = $row->tipo_edu;
        }
        
        $data['quienes'][''] = '-';
        $query = $this->db->get("quienes");
        foreach ($query->result() as $row) {
            $data['quienes'][$row->quien] = $row->quien;
        }
        
        $data['jornadas'][''] = '-';
        $query = $this->db->get("jornadas");
        foreach ($query->result() as $row) {
            $data['jornadas'][$row->id_jornada] = $row->jornada;
        }
        
        $query = $this->db->get("origenes");
        foreach ($query->result() as $row) {
            $data['origenes'][$row->origen] = $row->origen;
        }

        $data['edades']['-1'] = 'Sin dato';
        $data['edades']['0'] = 'Menor de 1 año';
        $data['edades']['1'] = '1 año';
        for ($i = 2; $i <= 99; $i++) {
            $data['edades'][$i] = $i." años";
        }
        
        $query = $this->db->get("diferencial");
        foreach ($query->result() as $row) {
            $data[$row->tipo_diferencial][$row->diferencial] = $row->diferencial;
        }

        $query = $this->db->get("regimen_salud");
         $data['regimen_salud'][''] = "-";
        foreach ($query->result() as $row) {
            $data['regimen_salud'][$row->regimen] = $row->regimen;
        }

        $query = $this->db->get("tipos_discapacidad");
         $data['discapacidad'][''] = "-";
        foreach ($query->result() as $row) {
            $data['discapacidad'][$row->discapacidad] = $row->discapacidad;
        }

        $query = $this->db->get("localidades");
        $data['localidades'][''] = "- sin dato -";
        foreach ($query->result() as $row) {
            $data['localidades'][$row->localidad_id] = $row->localidad;
        }

        $data['grados'][''] = "-";
        $data['grados']['0'] = "Preescolar";
        for ($i = 1; $i <= 11; $i++) {
            $data['grados'][$i] = $i."°";
        }

        $this->db->where('orden > 0');
		$this->db->order_by('orden');
		$query = $this->db->get("motivos");
        $i = 200;
        foreach ($query->result() as $row) {
            if ($row->nivel == 1) {
                $data['motivos'][$i++] = '&nbsp;';
                $espacio = '';
            } else {
                $espacio = '&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            if ($row->valido == "si") {
                $clave = $row->motivo_id;
            } else {
                $clave = $i++;
            }

            $data['motivos'][$clave] = $espacio.$row->codigo." ".$row->motivo;
        }

        $query = $this->db->get("lineas");
        $data['lineas'][''] = "Seleccione una...";
        foreach ($query->result() as $row) {
            $data['lineas'][$row->linea_id] = $row->linea;
        }

        $query = $this->db->get("respuesta");
        $data['respuesta'][''] = "Seleccione";
        foreach ($query->result() as $row) {
            $data['respuesta'][$row->respuesta_llamada_id] = $row->respuesta_llamada;
        }		
		
		$this->db->order_by('tipo_entidad');
        $query = $this->db->get("entidades");
        $data['entidades'][''] = "&nbsp;";
        foreach ($query->result() as $row) {
            $data['entidades'][$row->entidad] = $row->tipo_entidad.': '.$row->entidad;
        }

        $query = $this->db->get("comos");
        $data['comos'][''] = "&nbsp;";
        foreach ($query->result() as $row) {
            $data['comos'][$row->como_id] = $row->como;
        }

        $query = $this->db->get("etnias");
        $data['etnias'][''] = "&nbsp;";
        foreach ($query->result() as $row) {
            $data['etnias'][$row->etnia_id] = $row->etnia;
        }
        
        $query = $this->db->get("relaciones");
        $data['relaciones'][''] = "&nbsp;";
        foreach ($query->result() as $row) {
            $data['relaciones'][$row->id_relacion] = $row->relacion;
        }
        
        $query = $this->db->get("lugares");
        $data['lugares'][''] = "&nbsp;";
        foreach ($query->result() as $row) {
            $data['lugares'][$row->id_lugar] = $row->lugar;
        }
        
        $data['dias'][''] = "-";
        for ($i = 1; $i <= 31; $i++) {
            $data['dias'][str_pad($i, 2, "0", STR_PAD_LEFT)] = str_pad($i, 2, "0", STR_PAD_LEFT);
        }
        setlocale(LC_TIME, 'esp');
        $data['meses'][''] = "-";
        for ($i = 1; $i <= 12; $i++) {
            $data['meses'][str_pad($i, 2, "0", STR_PAD_LEFT)] = strftime("%B", mktime(0, 0, 0, $i, 1, 2000));
        }
        $data['annos'][''] = "-";
        for ($i = date('Y'); $i >= 1900; $i--) {
            $data['annos'][$i] = $i;
        }
        
        return $data;
    }
    
}