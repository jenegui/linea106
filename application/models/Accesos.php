<?php
/*
 * Modelo para consultar accesos de los usuarios
 */

class Accesos extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function ultimos ($usuario ='') {
        $lista ='';
        $this->db->where('usuario_acceso',$usuario);
        $this->db->order_by('id','desc');
        $query = $this->db->get("accesos", 15);
        foreach ($query->result() as $row) {
            echo '<small>*&nbsp;'.$row->hora_acceso.'<br/></small>';
        }
    }

    
}

