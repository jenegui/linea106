<h1><?php echo $titulo; ?><br/>
    del <?php echo $this->session->userdata('inicio'); ?>
    al <?php echo $this->session->userdata('final'); ?>
</h1>
<?php
if ($this->session->tipo_usuario != 'administrador') {
    echo'<h1>'.$this->session->nombre_usuario.' '.$this->session->apellido_usuario.'</h1>';
} else {
    echo'<h1>Todos los Profesionales</h1>';
}
?>
<?php $suma=0; $subsuma=0;?>
<table>
    <tr>
        <th><?php echo $columna; ?></th>
        <th>Sexo</th>
        <th>Edad</th>
        <th>Total</th>
    </tr>
    <?php foreach ($query->result() as $row):?>
    <?php 
    if (isset($anterior) and $anterior != (($row->concepto =='')  ? '??' :$row->concepto)) {
        echo '<tr><th colspan="3">Subtotal '.$anterior.'</th><th>'.$subsuma.'</th></tr>';
        $subsuma =0;
    }
    ?>
    <tr>
        <td align="left"><?php echo $row->concepto;?></td>
        <td align="center"><?php echo $row->sexo;?></td>
        <td align="center"><?php echo $row->rango;?></td>
        <td align="right"><?php echo number_format($row->total);?></td>
    </tr>
    <?php 
    $anterior = ($row->concepto =='')  ? '??' :$row->concepto;
    $suma +=$row->total; 
    $subsuma +=$row->total; 
    ?>
    <?php endforeach;?>

    <tr>
        <th colspan="3">Subtotal <?php echo $anterior;?></th>
        <th align="right"><?php echo ($subsuma);?></th>
    </tr>
    <tr>
        <th colspan="3">TOTAL</th>
        <th align="right"><?php echo number_format($suma);?></th>
    </tr>
</table>