<?php if($mensaje == "ok"): ?>

<html>
<head>
<title>Password cambiado</title>
<meta http-equiv="refresh" content="2; url=<?php echo base_url()?>">
</head>
<body
<h1>El nuevo password fue registrado exitosamente !!</h1>
</body>
</html>

<?php else: ?>

<h2>CAMBIO DE PASSWORD</h2>

<div style="text-align: center; color: red; font-weight: bold;"><big>
<?php echo $mensaje?></big></div>

<?php echo form_open('ingreso/cambiar')?>
<table border="0" align="center">
<tr>
<td>Usuario: </td>
<td><b><?php echo $usuario; ?></b></td>
</tr>

<tr>
<td>Nombre:</td>
<td><input name="nombres" type="text" size="30" maxlength="40" value="<?php echo $nombre_usuario?>"/></td>
</tr>

<tr>
<td>Apellido:</td>
<td><input name="apellidos" type="text" size="30" maxlength="40" value="<?php echo $apellido_usuario?>"/></td>
</tr>

<tr>
<td>Correo electrónico:</td>
<td><input name="correo" type="text" size="30" maxlength="40" value="<?php echo $correo?>"/></td>
</tr>

<tr>
<td>Password Anterior: </td>
<td><input name="oldpass" type="password" size="10" maxlength="20"/></td>
</tr>

<tr>
<td>Password Nuevo: </td>
<td><input name="newpass1" type="password" size="10" maxlength="20"/></td>
</tr>

<tr>
<td>Confirmar Password: </td>
<td><input name="newpass2" type="password" size="10" maxlength="20"/></td>
</tr>

<tr>
<td>
</td>
<td><input type="submit" name="ok" value="Aceptar"/></td>
</tr>

</table>
</form>

<?php endif; ?>
