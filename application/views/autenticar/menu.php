<!-- MENU -->
<br/>
<div style="text-align: center;">

<?php if ($this->session->userdata('tipo_usuario') == 'administrador'): ?>
    | <small><?php echo anchor('ingreso/nuevo', 'Administrar usuarios'); ?></small>
<?php endif;?>
    | <small><?php echo anchor('llamadas', 'Nueva llamada'); ?></small>
    | <small><?php echo anchor('llamadas/anteriores', 'Anteriores llamadas'); ?></small>
    | <small><?php echo anchor('seguimientos', 'Seguimientos'); ?></small>
    | <small><?php echo anchor('reportes', 'Reportes'); ?></small>
    | <small><?php echo anchor('ingreso/actualizar', 'Cambiar Password'); ?></small>
    | <small><?php echo anchor('ingreso', 'Salir'); ?></small>
    |
</div>
<br/>
<!-- FIN MENU -->