
<h1>Nuevo usuario</h1>

<h2 style="color:red;"><?php echo $mensaje;?></h2>

<?php echo form_open('ingreso/nuevo');?>
<table>
<tr>
<td>Usuario ID:</td>
<td><input name="usuario" type="text" size="10" maxlength="20" /></td>
</tr>

<tr>
<td>Nombre:</td>
<td><input name="nombre_usuario" type="text" size="30" maxlength="100" /></td>
</tr>

<tr>
<td>Apellido:</td>
<td><input name="apellido_usuario" type="text" size="30" maxlength="100" /></td>
</tr>

<tr>
<td>Correo:</td>
<td><input name="correo" type="text" size="30" maxlength="100" /></td>
</tr>

<tr>
<td>Cargo:</td>
<td><input name="cargo" type="text" size="30" maxlength="100" value="Profesional Línea 106"/></td>
</tr>

<tr>
<td>Tipo Usuario:</td>
<td>
<select name="tipo_usuario">
    <option value="profesional">Profesional</option>
    <option value="administrador">Administador</option>
</select>
</td>
</tr>

<tr>
<td></td>
<td><input type="submit" name="ok" value="Aceptar" /></td>
</tr>

</table>
<?php echo form_close('<br/>');?>

<h2>USUARIOS REGISTRADOS</h2>
<table>
    <tr>
        <th>id</th>
        <th>usuario</th>
        <th colspan='2'>Nombre</th>
        <th>Correo</th>
        <th>Cargo</th>
        <th>Tipo usuario</th>
        <th>Ultimos accesos</th>
        <th>Total accesos</th>
        <th colspan='3'>Clave</th>
    </tr>
    <?php foreach ($usuarios->result() as $row):?>
    <tr>
        <td><?php echo $row->id_usuario; ?></td>
        <td><?php echo $row->usuario; ?></td>
        <td><?php echo $row->nombre_usuario; ?></td>
        <td><?php echo $row->apellido_usuario; ?></td>
        <td><?php echo $row->correo; ?></td>
        <td><?php echo $row->cargo; ?></td>
        <td><?php echo $row->tipo_usuario; ?></td>
        <td><?php $this->Accesos->ultimos($row->usuario);?></td>
        <td><?php echo $row->total_accesos; ?></td>
        <?php if ($row->clave == "") {
            echo "<td>CLAVE ANULADA:<br/>".$row->ultimo_cambio."</td>";
        } elseif ($row->ultimo_cambio == "") {
            echo "<td>Clave = 1234</td>";
        } else {
            echo "<td>Actualizada:<br/>".$row->ultimo_cambio."</td> \n";
        } ?>
        <td>
            <?php if ($row->ultimo_cambio != ""): ?>
                <?php echo form_open('ingreso/reiniciar');?>
                <input type='hidden' name='id_usuario' value='<?php echo $row->id_usuario; ?>' />
                <input type='submit' name='ok' value='Reiniciar Clave'
                  onclick="return confirm('Esta seguro de reiniciar esta clave?')" />
                <?php echo form_close();?>
            <?php endif; ?>
        </td>
        <td>
            <?php if ($row->clave != ""): ?>
                <?php echo form_open('ingreso/anular');?>
                <input type='hidden' name='id_usuario' value='<?php echo $row->id_usuario; ?>' />
                <input type='submit' name='ok' value='Anular'
                  onclick="return confirm('Esta seguro de ANULAR esta clave?')" />
                <?php echo form_close();?>
            <?php endif; ?>
        </td>

    </tr>
    <?php endforeach;?>
</table>
