<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon" href="<?php echo base_url();?>images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" type="image/x-icon" />
    <title>:: Linea 106 ::</title>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8"/>
    <meta name="author" content="Juan Pablo Berdejo"/>
    <link href="<?php echo base_url()?>estilos.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table cellpadding="2" cellspacing="0" border="0" align='center'>
    <tr>
        <td class="blanco" align="center">
            <img src="<?php echo base_url();?>images/logo_sds.png" alt="Secretaria de Salud" />
        </td>
        <td class="blanco">
            <img src="<?php echo base_url();?>images/linea106.png" alt="Linea 106" />
        </td>
    </tr>
    <tr style="font-weight: bold;" align="right">
        <td class='blanco' colspan='2'>
            <span style="color: blue;">
                <?php echo $this->session->userdata('nombre_usuario')?>
                <?php echo $this->session->userdata('apellido_usuario')?>
            </span>
        </td>
    </tr>
</table>
<!-- FIN ENCABEZADO -->

