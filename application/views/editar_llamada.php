<script type="text/javascript">

function valida_llamada(){

    //VALIDAR NARRATIVA
    if (document.form_llamada.narrativa.value == ""){
        alert("Falta la narrativa !")
        document.form_llamada.narrativa.focus()
        return 0;
    }

    //VALIDAR INTERVENCION
    if (document.form_llamada.intervencion.value == ""){
        alert("Falta la intervención !")
        document.form_llamada.intervencion.focus()
        return 0;
    }

    //ENVIAR
    if (confirm('¿ Esta seguro de los datos que se van a almacenar ?')){
        document.form_llamada.submit();
    }
}
</script>


<h1>Registro de llamada</h1>

<?php echo form_open('llamadas/guardar_editado', array('name' => 'form_llamada'));?>

<table>
    <tr>
        <th colspan="2">IDENTIFICACIÓN Y CARACTERIZACIÓN</th>
    </tr>
    <tr>
        <td><strong>Hora de inicio</strong></td>
        <td><?php echo $llamada['hora_inicio']; ?></td>
    </tr>
    <tr>
        <td><strong>Origen</strong></td>
        <td><?php echo $llamada['origen']." ".$llamada['telefono']; ?></td>
    </tr>
    <tr>
        <td><strong>Documento de identidad</strong></td>
        <td>
            <?php echo $llamada['tipo_doc_id_llam']; ?>
            <?php echo $llamada['num_doc']; ?>
        </td>
    </tr>
    <tr>
        <td><strong>Nombre</strong></td>
        <td>
            <?php echo $llamada['primer_nombre']; ?>
            <?php echo $llamada['segundo_nombre']; ?>
            <?php echo $llamada['primer_apellido']; ?>
            <?php echo $llamada['segundo_apellido']; ?>
        </td>
    </tr>
    <tr>
        <td><strong>Edad</strong></td>
        <td><?php echo $llamada['edad']; ?> años</td>
    </tr>
    <tr>
        <td><strong>Sexo</strong></td>
        <td><?php echo $llamada['sexo']; ?></td>
    </tr>
    <tr>
        <td><strong>Localidad</strong></td>
        <td><?php echo $llamada['localidad']; ?></td>
    </tr>
    <tr>
        <td><strong>Cómo conoció la Línea 106</strong></td>
        <td><?php echo $llamada['como']; ?></td>
    </tr>
    <tr>
        <td><strong>Colegio o jardín</strong></td>
        <td>
            <?php echo $llamada['colegio_jardin']; ?>&nbsp;&nbsp;&nbsp;&nbsp;
            Sede: <?php echo $llamada['sede']; ?>&nbsp;&nbsp;&nbsp;&nbsp;
            Grado: <?php echo $llamada['grado']; ?>°
        </td>
    </tr>
    <tr>
        <td><strong>Población de interés</strong></td>
        <td>
            <?php
            echo ($llamada['gestante'] ==1)            ? "Gestante.<br/><br/>" : "";
            echo ($llamada['desplazado'] ==1)          ? "PSD (desplazamiento).<br/><br/>" : "";
            echo ($llamada['hab_calle'] ==1)           ? "Habitante de calle.<br/><br/>" : "";
            echo ($llamada['discapacidad'] ==1)        ? "Discapacidad.<br/><br/>" : "";
            echo ($llamada['lgbt'] ==1)                ? "L.G.B.T.<br/><br/>" : "";
            echo ($llamada['trabajador_infantil'] ==1) ? "Trabajador infantil.<br/><br/>" : "";
            echo ($llamada['no_escolarizado'] ==1)     ? "No escolarizado.<br/><br/>" : "";
            echo ($llamada['conflicto'] ==1)           ? "Riesgo o vinculación al conflicto armado.<br/><br/>" : "";
            echo ($llamada['ive'] ==1)                 ? "IVE (interrupción voluntaria del embarazo).<br/><br/>" : "";
            ; ?>
        </td>
    </tr>
    <tr>
        <td><strong>Grupo étnico</strong></td>
        <td><?php echo $llamada['etnia']; ?></td>
    </tr>

    <tr>
        <th colspan="2">CONSULTA</th>
    </tr>
    <tr>
        <td><strong>Motivo de consulta</strong></td>
        <td><?php echo $llamada['motivo']; ?></td>
    </tr>
    <tr>
        <td><strong>Narrativa</strong></td>
        <td><?php echo form_textarea('narrativa', $llamada['narrativa']); ?></td>
    </tr>
    <tr>
        <td><strong>Línea de intervención</strong></td>
        <td>
            <?php echo $llamada['linea']; ?>
            <?php echo $llamada['entidad']; ?>
        </td>
    </tr>
    <tr>
        <td><strong>Intervención</strong></td>
        <td><?php echo form_textarea('intervencion', $llamada['intervencion']); ?></td>
    </tr>
    <tr>
        <td><strong>Observaciones</strong></td>
        <td><?php echo form_textarea('observaciones', $llamada['observaciones']); ?></td>
    </tr>
    <tr>
        <th colspan="2">CIERRE</th>
    </tr>

    <tr>
        <td><strong>Hora de cierre</strong></td>
        <td><?php echo $llamada['hora_cierre']; ?></td>
    </tr>
    <tr>
        <td class="blanco" colspan="2" align="center">
            <input type="hidden" name="llamada_id" value="<?php echo $llamada['llamada_id']; ?>" />
            <input type="button" value="Guardar" onclick="valida_llamada();"/>
        </td>
    </tr>
</table>

<?php echo form_close('<br/><br/>');?>