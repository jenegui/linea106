<h1><?php echo $titulo; ?><br/>
    del <?php echo $this->session->userdata('inicio'); ?>
    al <?php echo $this->session->userdata('final'); ?>
</h1>
<?php $suma=0; ?>
<table>
    <tr>
        <th><?php echo $columna; ?></th>
        <th>Total</th>
    </tr>
    <?php foreach ($query->result() as $row):?>
    <tr>
        <td align="left">
            <?php echo $row->concepto;?>
        </td>
        <td align="right">
            <?php echo number_format($row->total);?>
        </td>
    </tr>
    <?php $suma +=$row->total; ?>
    <?php endforeach;?>

    <tr>
        <th>TOTAL</th>
        <th align="right"><?php echo number_format($suma);?></th>
    </tr>

</table>