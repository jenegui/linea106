<script type="text/javascript" src="<?php echo base_url()?>varios.js"></script>

<script type="text/javascript">

function valida_envia(){

    //Validar fecha
    if (document.form_seg.fecha_seg.value == ''){
        alert("Falta la fecha o es inválida !")
        document.form_seg.fecha_seg.focus()
        return false;
    }else if (!esFechaValida(document.form_seg.fecha_seg)){
        alert("Falta la fecha o es inválida !")
        document.form_seg.fecha_seg.focus()
        return false;
    }
    //Validar medio
    if (document.form_seg.medio.selectedIndex==0){
        alert("Debe seleccionar un medio !")
        document.form_seg.medio.focus()
        return false;
    }
    //Validar quien
    if (document.form_seg.quien.selectedIndex==0){
        alert("Debe seleccionar quien responde !")
        document.form_seg.quien.focus()
        return false;
    }
    //Validar descripción quien
    if (document.form_seg.descripcion_quien.value==""){
        alert("Falta describir quien !")
        document.form_seg.descripcion_quien.focus()
        return false;
    }
    //Validar observaciones
    if (document.form_seg.observaciones.value==""){
        alert("Faltan las observaciones !")
        document.form_seg.observaciones.focus()
        return false;
    }
    //Validar estado
    if (document.form_seg.estado.selectedIndex==0){
        alert("Debe seleccionar el estado !")
        document.form_seg.estado.focus()
        return false;
    }

    //ENVIAR
    return confirm('¿ Esta seguro de los datos que se van a almacenar ?');
}
</script>

<h1>Seguimientos registrados</h1>
<table>
    <tr>
        <th>ID</th>
        <th>Fecha de la respuesta</th>
        <th>Medio de llegada</th>
        <th>Quien responde</th>
        <th>Observaciones</th>
        <th>Estado</th>
        <th>Registrado</th>
    </tr>
<?php foreach ($seguimientos->result() as $row): ?>
    <tr>
        <td><?php echo $row->seg_id; ?></td>
        <td><?php echo $row->fecha_seg; ?></td>
        <td><?php echo $row->medio; ?></td>
        <td><?php echo $row->quien.": ".$row->descripcion_quien; ?></td>
        <td><?php echo $row->observaciones; ?></td>
        <td><?php echo $row->estado; ?></td>
        <td><?php echo $row->registrado_seg." ".$row->profesional_seg; ?></td>
    </tr>
<?php endforeach; ?>
</table>

<hr/>
<h1>Registrar nuevo seguimiento</h1>
<?php echo form_open('seguimientos/guardar', array('name' => 'form_seg', 'onsubmit' => 'return valida_envia();'));?>
<input type="hidden" name="llamada_id" value="<?php echo $llamada['llamada_id']; ?>"/>
<table>
    <tr>
        <td><strong>Fecha de la respuesta</strong></td>
        <td><input type="text" name="fecha_seg" size="10" maxlength="10" /> AAAA-MM-DD</td>
    </tr>
    <tr>
        <td><strong>Medio de llegada</strong></td>
        <td><?php echo form_dropdown('medio', $medios, ''); ?></td>
    </tr>
    <tr>
        <td><strong>Quien responde</strong></td>
        <td>
            <?php echo form_dropdown('quien', $quienes, ''); ?>
            Cual?: <input type="text" name="descripcion_quien" size="20" maxlength="255" />
        </td>
    </tr>
    <tr>
        <td><strong>Observaciones</strong></td>
        <td><textarea name="observaciones" cols="80" rows="10"></textarea></td>
    </tr>
    <tr>
        <td><strong>Estado</strong></td>
        <td><?php echo form_dropdown('estado', $estados, ''); ?></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><input type="submit" name="ok" value="Guardar" /></td>
    </tr>
</table>
<?php echo form_close(); ?>


<hr/>
<h1>Datos de la llamada</h1>
<table>
    <tr>
        <th colspan="2">LLAMADA</th>
    </tr>
    <tr>
        <td><strong>ID</strong></td>
        <td><?php echo $llamada['llamada_id']; ?></td>
    </tr>
    <tr>
        <td><strong>Hora de inicio</strong></td>
        <td><?php echo $llamada['hora_inicio']; ?></td>
    </tr>
    <tr>
        <td><strong>Hora de cierre</strong></td>
        <td><?php echo $llamada['hora_cierre']; ?></td>
    </tr>
    <tr>
        <td><strong>Profesional</strong></td>
        <td><?php echo $llamada['profesional']; ?></td>
    </tr>
    <tr>
        <td><strong>Origen</strong></td>
        <td><?php echo $llamada['origen']." ".$llamada['telefono']; ?></td>
    </tr>

    <tr>
        <th colspan="2">IDENTIFICACIÓN Y CARACTERIZACIÓN</th>
    </tr>
    <tr>
        <td><strong>Documento de identidad</strong></td>
        <td>
            <?php echo $llamada['tipo_doc_id_llam']; ?>
            <?php echo $llamada['num_doc']; ?>
        </td>
    </tr>
    <tr>
        <td><strong>Nombre</strong></td>
        <td>
            <?php echo $llamada['primer_nombre']; ?>
            <?php echo $llamada['segundo_nombre']; ?>
            <?php echo $llamada['primer_apellido']; ?>
            <?php echo $llamada['segundo_apellido']; ?>
        </td>
    </tr>
    <tr>
        <td><strong>Edad</strong></td>
        <td><?php echo $llamada['edad']; ?> años</td>
    </tr>
    <tr>
        <td><strong>Sexo</strong></td>
        <td><?php echo $llamada['sexo']; ?></td>
    </tr>
    <tr>
        <td><strong>Localidad</strong></td>
        <td><?php echo $llamada['localidad']; ?></td>
    </tr>
    <tr>
        <td><strong>Cómo conoció la Línea 106</strong></td>
        <td><?php echo $llamada['como']; ?></td>
    </tr>
    <tr>
        <td><strong>Colegio o jardín</strong></td>
        <td>
            <?php echo $llamada['colegio_jardin']; ?>&nbsp;&nbsp;&nbsp;&nbsp;
            Sede: <?php echo $llamada['sede']; ?>&nbsp;&nbsp;&nbsp;&nbsp;
            Grado: <?php echo $llamada['grado']; ?>°
        </td>
    </tr>
    <tr>
        <td><strong>Población de interés</strong></td>
        <td>
            <?php
            echo ($llamada['gestante'] ==1)            ? "Gestante.<br/><br/>" : "";
            echo ($llamada['desplazado'] ==1)          ? "PSD (desplazamiento).<br/><br/>" : "";
            echo ($llamada['hab_calle'] ==1)           ? "Habitante de calle.<br/><br/>" : "";
            echo ($llamada['discapacidad'] ==1)        ? "Discapacidad.<br/><br/>" : "";
            echo ($llamada['lgbt'] ==1)                ? "L.G.B.T.<br/><br/>" : "";
            echo ($llamada['trabajador_infantil'] ==1) ? "Trabajador infantil.<br/><br/>" : "";
            echo ($llamada['no_escolarizado'] ==1)     ? "No escolarizado.<br/><br/>" : "";
            echo ($llamada['conflicto'] ==1)           ? "Riesgo o vinculación al conflicto armado.<br/><br/>" : "";
            echo ($llamada['ive'] ==1)                 ? "IVE (interrupción voluntaria del embarazo).<br/><br/>" : "";
            ?>
        </td>
    </tr>
    <tr>
        <td><strong>Grupo étnico</strong></td>
        <td><?php echo $llamada['etnia']; ?></td>
    </tr>

    <tr>
        <th colspan="2">CONSULTA E INTERVENCIÓN</th>
    </tr>
    <tr>
        <td><strong>Motivo de consulta</strong></td>
        <td><?php echo $llamada['motivo']; ?></td>
    </tr>
    <tr>
        <td><strong>Narrativa</strong></td>
        <td><?php echo $llamada['narrativa']; ?></td>
    </tr>
    <tr>
        <td><strong>Línea de intervención</strong></td>
        <td>
            <?php echo $llamada['linea']; ?>
            <?php echo $llamada['entidad']; ?>
        </td>
    </tr>
    <tr>
        <td><strong>Intervención</strong></td>
        <td><?php echo $llamada['intervencion']; ?></td>
    </tr>
</table>