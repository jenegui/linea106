<h1>Buscar caso</h1>
<?php echo form_open('seguimientos');?>
<table class ="blanco">
    <tr>
        <td class ="blanco">Documento de identidad</td>
        <td class ="blanco"><?php echo form_input('num_doc', $num_doc);?></td>
    </tr>
    <tr>
        <td class ="blanco">Primer Nombre</td>
        <td class ="blanco"><?php echo form_input('nombre', $nombre);?></td>
    </tr>
    <tr>
        <td class ="blanco">Primer Apellido</td>
        <td class ="blanco"><?php echo form_input('apellido', $apellido);?></td>
    </tr>
    <tr>
        <td class ="blanco">Profesional</td>
        <td class ="blanco"><?php echo form_dropdown('profesional', $usuarios, $profesional);?></td>
    </tr>
    <tr>
        <td class ="blanco">Institución</td>
        <td class ="blanco"><?php echo form_dropdown('entidad', $entidades, $entidad);?></td>
    </tr>
    <tr>
        <td class ="blanco">Fechas</td>
        <td class="blanco">
            <input type="text" name="inicio" size="10" maxlength="10" value="<?php echo $inicio ;?>" /> al
            <input type="text" name="final" size="10" maxlength="10" value="<?php echo $final ;?>" />
        </td>
    </tr>
    <tr>
        <td class ="blanco" colspan="2" align="center">
            <input type="submit" name="ok" value="Buscar"/>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>

<?php if (isset($llamadas)): ?>
<h1>Registros encontrados</h1>
<table>
    <tr>
        <th>#</th>
        <th>Origen</th>
        <th>Identificación</th>
        <th>Sexo/Edad</th>
        <th>De interés</th>
        <th>Motivo</th>
        <th>Narrativa</th>
        <th>Línea de Interveción</th>
        <th>Entidad</th>
        <th>Intervención</th>
        <th>Registro</th>
    </tr>
    <?php foreach ($llamadas->result() as $row): ?>
    <tr>
        <td><?php echo $row->llamada_id;?></td>
        <td>
            <?php echo $row->origen;?><br/>
            <?php echo $row->telefono;?>
        </td>
        <td>
            <?php echo $row->primer_nombre;?>
            <?php echo $row->segundo_nombre;?>
            <?php echo $row->primer_apellido;?>
            <?php echo $row->segundo_apellido;?><br/>
            <small>(<?php echo $row->tipo_doc_id_llam.$row->num_doc;?>)</small>
        </td>
        <td>
            <?php echo $row->sexo;?><br/>
            <?php echo $row->edad;?> años
        </td>
        <td>
            <?php
            echo ($row->gestante ==1)            ? "Gestante.<br/><br/>" : "";
            echo ($row->desplazado ==1)          ? "PSD (desplazamiento).<br/><br/>" : "";
            echo ($row->hab_calle ==1)           ? "Habitante de calle.<br/><br/>" : "";
            echo ($row->discapacidad ==1)        ? "Discapacidad.<br/><br/>" : "";
            echo ($row->lgbt ==1)                ? "L.G.B.T.<br/><br/>" : "";
            echo ($row->trabajador_infantil ==1) ? "Trabajador infantil.<br/><br/>" : "";
            echo ($row->no_escolarizado ==1)     ? "No escolarizado.<br/><br/>" : "";
            echo ($row->conflicto ==1)           ? "Riesgo o vinculación al conflicto armado.<br/><br/>" : "";
            echo ($row->ive ==1)                 ? "IVE (interrupción voluntaria del embarazo).<br/><br/>" : "";
            ?>
        </td>

        <td><?php echo $row->motivo;?></td>
        <td><?php echo $row->narrativa;?></td>
        <td><?php echo $row->linea;?></td>
        <td><?php echo $row->entidad;?></td>
        <td><?php echo $row->intervencion;?></td>
        <td>
            <?php echo $row->profesional;?><br/>
            <?php echo $row->hora_registro;?>
        </td>
        <td>
            <?php echo form_open('seguimientos/capturar/'.$row->llamada_id); ?>
            <input type="hidden" name="llamada_id" value="<?php echo $row->llamada_id; ?>" />
            <input type="submit" value="Seleccionar" />
            <?php echo form_close()?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endif; ?>