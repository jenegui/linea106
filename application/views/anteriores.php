<?php echo form_open('llamadas/anteriores')?>
<table class="blanco">
   	<tr>
        <td class="blanco" align="right">ID Llamada: </td>
        <td class="blanco">
            <input type="text" name="llamada_id" size="11" maxlength="100" value="<?php echo $llamada_id;?>" />
			<input type="submit" value="Buscar" />
        </td>
    </tr>
    <tr>
        <td class ="blanco" align="center" colspan="2"><hr/></td>
    </tr>
		
	<tr>
        <td class="blanco" align="right">Nombre usuario: </td>
        <td class="blanco">
            <input type="text" name="palabra" size="20" maxlength="100" value="<?php echo $palabra;?>" />
            <input type="submit" value="Buscar" />
        </td>
    </tr>
    <tr>
        <td class ="blanco" align="center" colspan="2"><hr/></td>
    </tr>
    <tr>
        <td class="blanco" align="right">Teléfono:</td>
        <td class="blanco">
            <input type="text" name="telefono" size="10" maxlength="100" value="<?php echo $telefono;?>" />
        </td>
    </tr>
    <tr>
        <td class="blanco" align="right">Edad:</td>
        <td class="blanco">
            <?php echo form_dropdown('rango', $edades, $rango);?>
        </td>
    </tr>
    <tr>
        <td class="blanco" align="right">Fecha de la intervención:</td>
        <td class="blanco">
            <input type="text" name="inicio" size="10" maxlength="10" value="<?php echo $inicio ;?>" /> al
            <input type="text" name="final" size="10" maxlength="10" value="<?php echo $final ;?>" />
        </td>
    </tr>
    <tr>
        <td class="blanco" align="right">Línea de intervención:</td>
        <td class="blanco">
            <?php echo form_dropdown('linea_id', $lineas, $linea_id);?>
        </td>
    </tr>
    <tr>
        <td class="blanco" align="right">Institución a la que se hace la remisión:</td>
        <td class="blanco"><?php echo form_dropdown('entidad', $entidades, $entidad);?></td>
    </tr>
    <tr>
        <td class ="blanco" align="right">Profesional</td>
        <td class ="blanco"><?php echo form_dropdown('profesional', $usuarios, $profesional);?></td>
    </tr>
    <tr>
        <td class ="blanco" align="center" colspan="2"><input type="submit" value="Buscar" /><hr/></td>
    </tr>
    <tr>
        <td class="blanco" align="right">Elimina filtros:</td>
        <td class="blanco"><input type="submit" name="ok" value="Limpiar" /></td>
    </tr>
    
</table>
<?php echo form_close('<hr/>')?>

<?php echo $this->pagination->create_links();?>

<div style="text-align: center;">
    <small>Total registros encontrados: <?php echo $total; ?></small>
</div>

<table>
    <tr>
        <th>Nueva llamada del mismo caso</th>
        <th>#</th>
        <th>Inicio y cierre</th>
        <th>Origen</th>
        <th>Usuario</th>
        <th>Datos de quien se comunica</th>
        <th>De interés</th>
        <th>Localidad</th>
        <th>Ámbito Escolar</th>
        <th>Motivo</th>
        <th>Narrativa</th>
        <th>Línea</th>
        <th>Intervención</th>
        <th>Casos de violencia</th>
        <th>Observaciones</th>
        <th>Registro</th>
        <th>Editar</th>
    </tr>
    <?php foreach ($llamadas->result() as $row): ?>
    <tr>
        <td>
            <?php echo form_open('llamadas/mismo_caso/'.$row->llamada_id)?>
            <input type="hidden" name="llamada_id" value="<?php echo $row->llamada_id;?>" />
            <input type="submit" value="Nueva" />
            <?php echo form_close('<br/>')?>
        </td>
        <td><?php echo $row->llamada_id;?></td>
        <td>
            <?php echo $row->hora_inicio;?><hr/>
            <?php echo $row->hora_cierre;?>
        </td>
        <td>
            <?php 
            echo $row->origen.'<br/>';
            echo $row->telefono;
            if ($row->antiguo ==1) echo "<hr/>Usuario antiguo" ;
            ?>
        </td>
        <td>
            <?php 
            echo $row->primer_nombre.' ';
            echo $row->segundo_nombre.' ';
            echo $row->primer_apellido.' ';
            echo $row->segundo_apellido.'<br/>';
            if ($row->nombre_identitario !='') echo 'NomIdt:&nbsp;'.$row->nombre_identitario.'<br/>';
            echo $row->tipo_doc_id_llam.$row->num_doc.'<br/>';
            echo 'S:'.$row->sexo.'<br/>';
            if ($row->genero !='') echo 'G:'.$row->genero.'<br/>';
            if ($row->orientacion !='') echo 'OS:'.$row->orientacion.'<br/>';
            if ($row->edad >0) echo $row->edad.' años';
            ?>
        </td>
        <td>
            <?php 
            echo $row->primer_nombre_otro.' ';
            echo $row->segundo_nombre_otro.' ';
            echo $row->primer_apellido_otro.' ';
            echo $row->segundo_apellido_otro.'<br/>';
            echo $row->tipo_doc_id_llam_otro.$row->num_doc_otro.'<br/>';
            echo $row->sexo_otro.'<br/>';
            if ($row->edad_otro >0) echo $row->edad_otro.' años';
            ?>
        </td>
        <td>
            <?php
            echo ($row->gestante ==1)            ? "Gestante.<br/><br/>" : "";
            echo ($row->desplazado ==1)          ? "PSD (desplazamiento).<br/><br/>" : "";
            echo ($row->hab_calle ==1)           ? "Habitante de calle.<br/><br/>" : "";
            echo ($row->discapacidad ==1)        ? "Discapacidad.<br/><br/>" : "";
            echo ($row->lgbt ==1)                ? "L.G.B.T.<br/><br/>" : "";
            echo ($row->trabajador_infantil ==1) ? "Trabajador infantil.<br/><br/>" : "";
            echo ($row->no_escolarizado ==1)     ? "No escolarizado.<br/><br/>" : "";
            echo ($row->conflicto ==1)           ? "Riesgo o vinculación al conflicto armado.<br/><br/>" : "";
            echo ($row->tdah ==1)                ? "Trastorno por déficit de atención con hiperactividad.<br/><br/>" : "";
            echo ($row->ive ==1)                 ? "IVE (interrupción voluntaria del embarazo).<br/><br/>" : "";
            echo ($row->navidad ==1)             ? "Prevención Temporada Navideña" : "";
            ?>
        </td>
        <td><?php echo $row->localidad;?></td>
        <td>
            <?php if ($row->colegio_jardin != ""): ?>
                <?php echo $row->colegio_jardin;?>
                Sede:<?php echo $row->sede;?>
                Grado:<?php echo $row->grado;?>°
            <?php endif; ?>
        </td>
        <td><?php echo $row->motivo;?></td>
        <td><?php echo $row->narrativa;?></td>
        <td>
            <?php echo $row->linea;?>
            <?php echo $row->entidad;?>
        </td>
        <td><?php   
        if ($row->tema_semana !="") echo "TEMA DE LA SEMANA: ".$row->tema_semana."<hr/>" ;
        echo $row->intervencion;
        ?></td>
        <td><?php
        if ($row->id_violencia >0) {
            echo '<b>Víctima:</b>'.$row->primer_nombre_victima.' '.$row->segundo_nombre_victima.' '.$row->primer_apellido_victima.' '.$row->segundo_apellido_victima.'<br/>';
            if ($row->edad_victima >0) echo $row->edad_victima.'&nbsp;años<br/>';
            if ($row->sexo_victima !='') echo $row->sexo_victima.'<br/>';
            if ($row->direccion_victima !='') echo 'Dir: '.$row->direccion_victima.'<br/>';
            if ($row->telefono_victima !='') echo 'Tel: '.$row->telefono_victima.'<br/>';
            echo '<br/><b>Agresor:</b> '.$row->primer_nombre_agresor.' '.$row->segundo_nombre_agresor.' '.$row->primer_apellido_agresor.' '.$row->segundo_apellido_agresor.'<br/>';
            if ($row->edad_agresor >0) echo $row->edad_agresor.'&nbsp;años<br/>';
            if ($row->sexo_agresor !='') echo $row->sexo_agresor.'<br/>';
            if ($row->direccion_agresor !='') echo 'Dir: '.$row->direccion_agresor.'<br/>';
            if ($row->telefono_agresor !='') echo 'Tel: '.$row->telefono_agresor.'<br/>';
            echo 'Es: '.$row->relacion.'<br/>';
            echo '<br/><b>Ocurrió en:</b> '.$row->lugar;
        }
        ?></td>
        <td><?php echo $row->observaciones;?></td>
        <td>
            <?php echo $row->profesional;?><br/>
            <?php echo $row->hora_registro;?>
        </td>
        <?php if($row->profesional == $this->session->userdata('usuario')):?>
        <td>
            <?php echo form_open('llamadas/editar/'.$row->llamada_id)?>
            <input type="hidden" name="llamada_id" value="<?php echo $row->llamada_id;?>" />
            <input type="submit" value="Editar" />
            <?php echo form_close('<br/>')?>
        </td>
        <?php endif;?>
    </tr>
    <?php endforeach; ?>

</table>

<?php echo $this->pagination->create_links();?>



