<h1>Seleccione el Reporte</h1>
<h2 style="color:red;"><?php echo $mensaje;?></h2>

<?php echo form_open('reportes/validar');?>
<table>
    <tr>
        <td>Fecha inicial del reporte</td>
        <td>
            <input type="text" name="inicio" size="10" maxlength="10" value="<?php echo $inicio ;?>" />
            AAAA-MM-DD
        </td>
    </tr>

    <tr>
        <td>Fecha final del reporte</td>
        <td>
            <input type="text" name="final" size="10" maxlength="10" value="<?php echo $final ;?>" />
            AAAA-MM-DD
        </td>
    </tr>
    <tr>
        <td>Tipo de reporte</td>
        <td>
            <input type="radio" name="tipo" value="linea" />Por línea de intervención <br/>
            <input type="radio" name="tipo" value="motivo" />Por motivo de consulta <br/>
            <!--- input type="radio" name="tipo" value="agrupado" />Por motivos agrupados <br/> --->
            <input type="radio" name="tipo" value="localidad" />Por localidad <br/>
            <input type="radio" name="tipo" value="llamadas" />Intervenciones por profesional <br/>
            <input type="radio" name="tipo" value="entidad" />Por entidad a la que se remite <br/>
            <input type="radio" name="tipo" value="motivos_profesional" />Motivo de consulta por profesional <br/>
            <input type="radio" name="tipo" value="lineas_profesional" />Línea de intervención por profesional <br/>
            <input type="radio" name="tipo" value="poblacion" />Población
            <select name="poblacion">
                <option value="gestante">Gestante</option>
                <option value="desplazado">PSD (desplazamiento)</option>
                <option value="hab_calle">Habitante de calle</option>
                <option value="discapacidad">Discapacidad</option>
                <option value="lgbt">LGBTI</option>
                <option value="trabajador_infantil">Trabajador infantil</option>
                <option value="no_escolarizado">No escolarizado</option>
                <option value="conflicto">Riesgo o vinculación al conflicto armado</option>
                <option value="ive">IVE (interrupción voluntaria del embarazo) *</option>
            </select><br/>
			<?php if ($this->session->userdata('tipo_usuario') =='administrador') :?>  
				<input type="radio" name="tipo" value="exportar" /><b>Reporte total del período</b><br/>
				<input type="radio" name="tipo" value="exportar_monitoreo" /><b>Monitoreo de las atenciones</b>    
			<?php else :?> 
				<input type="radio" name="tipo" value="exportar_resumen" /><b>Reporte total del período</b><br/>
			<?php endif;?>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <input type="submit" value="Generar" />
        </td>
    </tr>
</table>

<?php echo form_close('<br/><br/>');?>