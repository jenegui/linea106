<script type="text/javascript">

function calcAge() {
    var birthday = +new Date(document.form_llamada.a_N.value,document.form_llamada.m_N.value,document.form_llamada.d_N.value);
    var edad =  ~~((Date.now() - birthday) / (31557600000));
    document.form_llamada.edad.value = edad;
    poblarDocTip();
    alert(edad +' Años');
}

function poblarDocTip() {
    var lista = document.form_llamada.edad;
    i = document.form_llamada.edad.selectedIndex;
    if (i != 0) {
            var dropdownObjectPath = document.form_llamada.tipo_doc_id_llam;
            var wichDropdown = "tipo_doc_id_llam";
            var withWhat = lista.options[lista.selectedIndex].value;

            populateOptions(wichDropdown, withWhat);
	}
}

function populateOptions(wichDropdown, withWhat) 
{
    o = new Array;
    i=0;

    if (withWhat == "0") {
        o[i++]=new Option('', '0');  
    }
    if (withWhat >= 18) {
        o[i++]=new Option('Cédula de ciudadanía','CC'); 
        o[i++]=new Option('Cédula de extranjería','CE'); 
        o[i++]=new Option('Adulto sin identificar','AS');
        o[i++]=new Option('Registro especial de permanencia','RP'); 
        o[i++]=new Option('NUIP','NP'); 
    }         
    if (withWhat <= 18) {
        o[i++]=new Option('Registro civil','RC'); 
        o[i++]=new Option('Tarjeta de identidad','TI'); 
        o[i++]=new Option('Pasaporte','PA'); 
        o[i++]=new Option('Menor sin identificar','MS'); 
        o[i++]=new Option('Registro especial de permanencia','RP'); 
        o[i++]=new Option('NUIP','NP');
    } 

    if (i==0) {
            alert(i + " " + "Error!!!");
            }
    else{
            dropdownObjectPath = document.form_llamada.tipo_doc_id_llam;
            eval(document.form_llamada.tipo_doc_id_llam.length=o.length);
            largestwidth=0;
            for (i=0; i < o.length; i++) 
                    {
                      eval(document.form_llamada.tipo_doc_id_llam.options[i]=o[i]);
                      if (o[i].text.length > largestwidth) {
                         largestwidth=o[i].text.length;    }
            }
            eval(document.form_llamada.tipo_doc_id_llam.length=o.length);
            //eval(document.myform.munid.options[0].selected=true);
    }

}

function activar_colegio(){
    if (document.form_llamada.como_id_llam.value == "2"){
        document.form_llamada.colegio_jardin.disabled=false
        document.form_llamada.sede.disabled=false
        document.form_llamada.grado.disabled=false
        document.form_llamada.localidad_colegio.disabled=false
    } else {
        document.form_llamada.colegio_jardin.disabled=true
        document.form_llamada.sede.disabled=true
        document.form_llamada.grado.disabled=true
        document.form_llamada.localidad_colegio.disabled=true
    }
}

function activar_entidades(){
    if (document.form_llamada.linea_id_llam.value == "4" ||
        document.form_llamada.linea_id_llam.value == "5"){
        document.form_llamada.entidad.disabled=false
        document.form_llamada.otra_entidad.disabled=false
		document.form_llamada.respuesta_llamada_id.disabled=true
    } else if (document.form_llamada.linea_id_llam.value == "8") {
		document.form_llamada.entidad.disabled=true
        document.form_llamada.otra_entidad.disabled=true
		document.form_llamada.respuesta_llamada_id.disabled=false
	} else {
        document.form_llamada.entidad.disabled=true
        document.form_llamada.otra_entidad.disabled=true
		document.form_llamada.respuesta_llamada_id.disabled=true
    }
}

function activar_edad(){
    document.getElementById("edad").style.display="table-row";
    document.getElementById("fec_nac").style.display="none";
}

function activar_otro(){
    document.getElementById("otro").style.display="table";
    document.getElementById("quien").style.display="none";
}

function activar_discapacidad(){
   
    if (document.form_llamada.discapacidad.value == "1"){
        document.getElementById("discapacidades").style.display="table-row";
    }else{
        document.form_llamada.discapacidades.disabled=false;
    }
}


function activar_violencia(){
    if ((document.form_llamada.motivo_id_llam.value > 2 && document.form_llamada.motivo_id_llam.value < 14) ||
    (document.form_llamada.motivo_id_llam.value > 63 && document.form_llamada.motivo_id_llam.value < 66)){
		document.getElementById("violencia").style.display="table-row";
    } else {
		document.getElementById("violencia").style.display="none";
    }
}


function valida_llamada(){

    //VALIDAR TELEFONO
    if (document.form_llamada.origen.selectedIndex==0 && document.form_llamada.telefono.value == ""){
        alert("Debe anotar el número de teléfono !")
        document.form_llamada.telefono.focus()
        return false;
    }

    //VALIDAR SEXO
    if (document.form_llamada.sexo.selectedIndex==0){
        alert("Debe seleccionar el sexo !")
        document.form_llamada.sexo.focus()
        return false;
    }

    //VALIDAR GENERO
    if (document.form_llamada.genero.selectedIndex==0){
        alert("Debe seleccionar el genero !")
        document.form_llamada.genero.focus()
        return false;
    }
    
    //VALIDAR EDAD
    if (document.form_llamada.edad.value<0){
        alert("Debe seleccionar la fecha de nacimiento o la edad !")
        return false;
    }

    //VALIDAR PRIMER NOMBRE Y APELLIDO SI ES REMISIÓN
    if (document.form_llamada.linea_id_llam.value==5
        && document.form_llamada.primer_nombre.value==""){
        alert("Debe incluir el primer nombre cuando es una remisión !")
        document.form_llamada.primer_nombre.focus()
        return false;
    }
    if (document.form_llamada.linea_id_llam.value==5
        && document.form_llamada.primer_apellido.value==""){
        alert("Debe incluir el primer apellido cuando es una remisión !")
        document.form_llamada.primer_apellido.focus()
        return false;
    }

    //VALIDAR MOTIVO DE CONSULTA
    if (document.form_llamada.motivo_id_llam.value > 199){
        alert("Debe seleccionar un motivo de consulta !")
        document.form_llamada.motivo_id_llam.focus()
        return false;
    }
    
    if ((document.form_llamada.motivo_id_llam.value > 2 && document.form_llamada.motivo_id_llam.value < 14) ||
    (document.form_llamada.motivo_id_llam.value > 63 && document.form_llamada.motivo_id_llam.value < 66)){
        if (document.form_llamada.id_relacion_agresor_victima.selectedIndex==0){
            alert("Debe seleccionar la Relación del agresor con la víctima !");
            document.form_llamada.id_relacion_agresor_victima.focus();
            return false;
        }
        if (document.form_llamada.id_lugar_ocurre.selectedIndex==0){
            alert("Debe seleccionar el Lugar de Ocurrencia !");
            document.form_llamada.id_lugar_ocurre.focus();
            return false;
        }
    }

    //VALIDAR LINEA INTERVENCION
    if (document.form_llamada.linea_id_llam.selectedIndex==0){
        alert("Debe seleccionar la línea de intervención !")
        document.form_llamada.linea_id_llam.focus()
        return false;
    }

    //VALIDAR ENTIDAD
    if (document.form_llamada.linea_id_llam.value == "4" ||
        document.form_llamada.linea_id_llam.value == "5"){
        if (document.form_llamada.entidad.value == "" &&
            document.form_llamada.otra_entidad.value == ""){
            alert("Falta la entidad de referenciación o remisión !")
            document.form_llamada.entidad.focus()
            return false;
        }
    }

    //VALIDAR HORA
    if (document.form_llamada.hora_inicio.value == document.form_llamada.hora_cierre.value){
        alert("La hora de cierre es igual a la de inicio !")
        document.form_llamada.hora_cierre.focus()
        return false;
    }

    //ENVIAR
    return confirm('¿ Esta seguro de los datos que se van a almacenar ?');
}
</script>


<h1>Registro de llamada</h1>

<?php echo form_open('llamadas/guardar', array('name' => 'form_llamada', 'onsubmit' => 'return valida_llamada();'));?>

<table>
    <tr>
        <th colspan="2">Datos llamada</th>
    </tr>
    <tr>
        <td><strong>Fecha</strong></td>
        <td><input type="text" name="fecha" value="<?php echo $fecha;?>"  size="10" disabled="disabled" /></td>
    </tr>
    <tr>
        <td><strong>Hora de inicio</strong></td>
        <td><input type="text" name="hora_inicio" value="<?php echo $hora;?>"  size="5" maxlength="5" readonly="readonly" /></td>
    </tr>
    <tr>
        <td><strong>Origen</strong></td>
        <td><table style="background: rgb(204, 230, 255); margin-left: 0px; margin-right: 0px;">
                <tr>
                    <td>
                        <?php echo form_dropdown('origen', $origenes, $origen);?>
                        <br/>Número de Teléfono:
                        <?php echo form_input(array('name'=>'telefono','value'=>$telefono,'size'=>"10", 'maxlength'=>"100"));?><br/>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <strong>Llamada de:</strong><br/>
                        <input type="radio" name="antiguo" value="" <?php if ($antiguo != '1') echo 'checked="checked"';?> /> Usuario nuevo<br/>
                        <input type="radio" name="antiguo" value="1" <?php if ($antiguo == '1') echo 'checked="checked"';?> /> Usuario antiguo
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="quien">
        <td><strong>Quien se comunica habla de?</strong></td>
        <td><select id="quien_llama" onchange="activar_otro();">
            <option value="NNA">Si mismo</option>
            <option value="OTRO">Otra persona</option>
            </select>
        </td>
    </tr>
</table>
<br/>    
<table id="otro" style="display:none;">
    <tr>
        <th colspan="2">Identificación de quien se contacta con Linea 106 (Diferente al usuario)</th>
    </tr>
    <tr>
        <td><strong>Documento de identidad de<br/>quien se comunica</strong></td>
        <td>
            <?php echo form_dropdown('tipo_doc_id_llam_otro', $documentos, $tipo_doc_id_llam_otro);?>
            <?php echo form_input(array('name'=>'num_doc_otro','value'=>$num_doc_otro,'size'=>"10", 'maxlength'=>"20"));?><br/>
        </td>
    </tr>
    <tr>
        <td><strong>Nombre de<br/>quien se comunica</strong></td>
        <td>
            Nombre 1: <?php echo form_input(array('name'=>'primer_nombre_otro','value'=>$primer_nombre_otro,'size'=>"10", 'maxlength'=>"30"));?>&nbsp
            Nombre 2: <?php echo form_input(array('name'=>'segundo_nombre_otro','value'=>$segundo_nombre_otro,'size'=>"10", 'maxlength'=>"30"));?><br/>
            Apellido 1: <?php echo form_input(array('name'=>'primer_apellido_otro','value'=>$primer_apellido_otro,'size'=>"10", 'maxlength'=>"30"));?>&nbsp;
            Apellido 2: <?php echo form_input(array('name'=>'segundo_apellido_otro','value'=>$segundo_apellido_otro,'size'=>"10", 'maxlength'=>"30"));?>
        </td>
    </tr>
    <tr>
        <td><strong>Edad de<br/>quien se comunica</strong></td>
        <td><?php echo form_dropdown('edad_otro', $edades, $edad);?></td>
    </tr>
    <tr>
        <td><strong>Relación con el NNA de<br/>quien se comunica</strong></td>
        <td><?php echo form_dropdown('quien_otro', $quienes, $quien_otro);?></td>
    </tr>
</table>
<br/>    
<table>    
    <tr>
        <th colspan="2">Identificación y caracterización del usuario</th>
    </tr>
    <tr>
        <td><strong>Nombre</strong></td>
        <td>
            Nombre 1: <?php echo form_input(array('name'=>'primer_nombre','value'=>$primer_nombre,'size'=>"10", 'maxlength'=>"30"));?>&nbsp
            Nombre 2: <?php echo form_input(array('name'=>'segundo_nombre','value'=>$segundo_nombre,'size'=>"10", 'maxlength'=>"30"));?><br/>
            Apellido 1: <?php echo form_input(array('name'=>'primer_apellido','value'=>$primer_apellido,'size'=>"10", 'maxlength'=>"30"));?>&nbsp;
            Apellido 2: <?php echo form_input(array('name'=>'segundo_apellido','value'=>$segundo_apellido,'size'=>"10", 'maxlength'=>"30"));?>
        </td>
    </tr>
    <!--<tr>
        <td><strong>Nombre identitario</strong></td>
        <td>
            <?php //echo form_input(array('name'=>'nombre_identitario','value'=>$nombre_identitario,'size'=>"50", 'maxlength'=>"50"));?><br/>
            <small>Nombre que usualmente usa en caso de haberse identificado como transgenerista.</small>
        </td>
    </tr>-->
    <tr  id="fec_nac" >
        <td><strong>Fecha de nacimiento</strong></td>
         <td><?php echo form_dropdown('d_N', $dias, $d_N)
                .form_dropdown('m_N', $meses, $m_N)
                .form_dropdown('a_N', $annos, $a_N,'onchange="calcAge();"'); ?>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <input type="checkbox" name="sin_FN" value="1" onchange="activar_edad();" />
             Sin dato (registrar edad)
         </td>
    </tr>
    <tr  id="edad" style="display:none;" onchange="poblarDocTip();">
        <td><strong>Edad</strong></td>
        <td><?php echo form_dropdown('edad', $edades, $edad);?></td>
    </tr>
    <tr>
        <td><strong>Documento de identidad</strong></td>
        <td>
            <?php echo form_dropdown('tipo_doc_id_llam', array('-'=>'Falta la fecha de nacimiento o la edad'), $tipo_doc_id_llam);?>
            <?php echo form_input(array('name'=>'num_doc','value'=>$num_doc,'size'=>"10", 'maxlength'=>"20"));?><br/>
        </td>
    </tr>
     <tr>
        <td><strong>Régimen de aseguramiento en salud </strong></td>
        <td>
            <?php echo form_dropdown('regimen_salud', $regimen_salud);?>
            <br/>
        </td>
    </tr>
    <tr>
        <td><strong>Sexo</strong></td>
        <td><?php echo form_dropdown('sexo', $sexos, $sexo);?></td>
    </tr>
    <tr>
        <td><strong>Género</strong></td>
        <td><?php echo form_dropdown('genero', $generos, $genero);?></td>
    </tr>
    <tr>
        <td><strong>Orientación sexual</strong></td>
        <td><?php echo form_dropdown('orientacion', $orientaciones, $orientacion);?></td>
    </tr>
    <tr>
        <td><strong>Localidad de la llamada</strong></td>
        <td><?php echo form_dropdown('localidad_id_llam', $localidades, $localidad_id_llam);?></td>
    </tr>
    <tr>
        <td><strong>Cómo conoció la Línea 106</strong></td>
        <td><?php echo form_dropdown('como_id_llam', $comos, $como_id_llam);?></td>
    </tr>
    <tr>
        <td><strong>Institución Educativa</strong></td>
        <td>
            <?php echo form_input(array('name'=>'colegio_jardin','value'=>$colegio_jardin,'size'=>"50", 'maxlength'=>"255"));?>
        </td>
    </tr>
    <tr>
        <td><strong>Datos de la institución educativa</strong></td>
        <td>
            Sede: <?php echo form_input(array('name'=>'sede','value'=>$sede,'size'=>"10", 'maxlength'=>"255"));?>&nbsp;&nbsp;
            Grado: <?php echo form_dropdown('grado', $grados, $grado);?>&nbsp;&nbsp;
            Jornada: <?php echo form_dropdown('id_jornada_llam', $jornadas, $id_jornada_llam);?>
            <br/>Localidad: <?php echo form_dropdown('localidad_colegio', $localidades, $localidad_colegio);?>&nbsp;&nbsp;
            Tipo: <?php echo form_dropdown('id_tipo_edu_llam', $tipos_edu, $id_tipo_edu_llam);?>
        </td>
    </tr>
    <tr>
        <td><strong>Poblaciones de interés</strong></td>
        <td>
            <?php echo form_checkbox('hab_calle', '1', ($hab_calle =='1') );?>
            Persona en situación de calle<br/>
            <?php echo form_checkbox('priv_libertad', '1', ($priv_libertad =='1') );?>
            Persona privada de la libertad<br/>
            <?php echo form_checkbox('inmigrante', '1', ($inmigrante =='1') );?>
            Persona inmigrante<br/>
            <?php echo form_checkbox('discapacidad', '1', ($discapacidad =='1'), 'onchange="activar_discapacidad();"' );?>
            Discapacidad<br/>
            <?php echo form_checkbox('conflicto', '1', ($conflicto =='1') );?>
            Personas en víctimas del conflicto armado<br/>
            <?php echo form_checkbox('reincorporacion', '1', ($reincorporacion =='1') );?>
            Personas en procesos de reincorporación a la sociedad civil<br/>
            <?php echo form_checkbox('gestante', '1', ($gestante =='1') );?>
            Gestante<br/>
                        
            
        </td>
    </tr>
    <tr id="discapacidades" style="display:none;" onchange="activar_discapacidad();">
        <td>
            <strong>Tipos de discapacidad</strong>
        </td>
        <td>
            <?php echo form_dropdown('tipo_discapacidad', $discapacidad);?>
            <br/>
        </td>
    </tr>
    <tr>
        <td><strong>Tema de interés</strong></td>
        <td>
            <!--<?php //echo form_checkbox('ive', '1', ($ive =='1') );?>
            IVE (interrupción voluntaria del embarazo)<br/>-->
            <?php echo form_checkbox('navidad', '1', ($navidad =='1') );?>
            Prevención Temporada Navideña
        </td>
    </tr>
    <tr>
        <td><strong>Grupo étnico</strong></td>
        <td><?php echo form_dropdown('etnia_id_llam', $etnias, $etnia_id_llam);?></td>
    </tr>
</table>
<br/>    
<table>
    <tr>
        <th colspan="2">CONSULTA</th>
    </tr>
    <!--<tr>
        <td><strong>Tema de la semana</strong></td>
        <td>
            <?php //echo form_input(array('name'=>'tema_semana','value'=>$tema_semana,'size'=>"50", 'maxlength'=>"255"));?>
        </td>
    </tr>-->
    <tr>
        <td><strong>Motivo de consulta</strong></td>
        <td><?php echo form_dropdown('motivo_id_llam', $motivos, $motivo_id_llam, 'onchange="activar_violencia();"');?></td>
    </tr>
    <tr>
        <td><strong>Motivo relacionado</strong></td>
        <td><?php echo form_dropdown('motivo_2', $motivos, $motivo_2);?></td>
    </tr>
    <tr>
        <td><strong>Motivo relacionado</strong></td>
        <td><?php echo form_dropdown('motivo_3', $motivos, $motivo_3);?></td>
    </tr>
    <tr>
        <td><strong>Narrativa</strong></td>
        <td><?php echo form_textarea(array('name'=>'narrativa', 'value'=>$narrativa, 'cols'=>'80', 'rows'=>'10'));?></td>
    </tr>
    
	<tr id="violencia" <?php echo ($linea_id_llam == '4' || $linea_id_llam == '5') ? 'style="display:table-row;"' : 'style="display:none;"' ?>>
	    <td><strong>Caso de Violencia</strong></td>
		<td>
			<table style="background: rgb(204, 230, 255);">
                <tr>
                    <td><b>Víctima</b></td>
                    <td>Nombres víctima:</td>
                    <td>
                        <?php echo form_input(array('name'=>'primer_nombre_victima','value'=>$primer_nombre_victima,'size'=>"10", 'maxlength'=>"50"));?>
                        <?php echo form_input(array('name'=>'segundo_nombre_victima','value'=>$segundo_nombre_victima,'size'=>"10", 'maxlength'=>"50"));?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Apellidos víctima:</td>
                    <td>
                        <?php echo form_input(array('name'=>'primer_apellido_victima','value'=>$primer_apellido_victima,'size'=>"10", 'maxlength'=>"50"));?>
                        <?php echo form_input(array('name'=>'segundo_apellido_victima','value'=>$segundo_apellido_victima,'size'=>"10", 'maxlength'=>"30"));?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Edad víctima:</td>
                    <td>
                        <?php echo form_dropdown('edad_victima', $edades, $edad_victima);?>
                        <?php echo form_dropdown('sexo_victima', $sexos, $sexo_victima);?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Dirección víctima:</td>
                    <td><?php echo form_input(array('name'=>'direccion_victima','value'=>$direccion_victima,'size'=>"30", 'maxlength'=>"150"));?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Teléfono víctima:</td>
                    <td><?php echo form_input(array('name'=>'telefono_victima','value'=>$telefono_victima,'size'=>"8", 'maxlength'=>"50"));?></td>
                </tr>
                <tr>
                <tr>
                    <td></td>
                    <td>Barrio víctima:</td>
                    <td><?php echo form_input(array('name'=>'barrio_victima','value'=>$barrio_victima,'size'=>"20", 'maxlength'=>"200"));?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Localidad víctima:</td>
                    <td><?php echo form_dropdown('localidad_victima', $localidades, $localidad_victima);?></td>
                </tr>
                <tr>
                    <td><b>Ofensor</b></td>
                    <td>Nombres ofensor:</td>
                    <td>
                        <?php echo form_input(array('name'=>'primer_nombre_agresor','value'=>$primer_nombre_agresor,'size'=>"10", 'maxlength'=>"50"));?>
                        <?php echo form_input(array('name'=>'segundo_nombre_agresor','value'=>$segundo_nombre_agresor,'size'=>"10", 'maxlength'=>"50"));?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Apellidos ofensor:</td>
                    <td>
                        <?php echo form_input(array('name'=>'primer_apellido_agresor','value'=>$primer_apellido_agresor,'size'=>"10", 'maxlength'=>"50"));?>
                        <?php echo form_input(array('name'=>'segundo_apellido_agresor','value'=>$segundo_apellido_agresor,'size'=>"10", 'maxlength'=>"30"));?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Edad y sexo ofensor:</td>
                    <td>
                        <?php echo form_dropdown('edad_agresor', $edades, $edad_agresor);?>
                        <?php echo form_dropdown('sexo_agresor', $sexos, $sexo_agresor);?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Dirección ofensor:</td>
                    <td><?php echo form_input(array('name'=>'direccion_agresor','value'=>$direccion_agresor,'size'=>"30", 'maxlength'=>"150"));?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Teléfono ofensor:</td>
                    <td><?php echo form_input(array('name'=>'telefono_agresor','value'=>$telefono_agresor,'size'=>"8", 'maxlength'=>"50"));?></td>
                </tr>
                <tr>
                    <td><b>Relación del ofensor<br/>con la víctima</b></td>
                    <td><?php echo form_dropdown('id_relacion_agresor_victima', $relaciones, $id_relacion_agresor_victima);?></td>
                </tr>
                <tr>
                    <td><b>Lugar de ocurrencia</b></td>
                    <td><?php echo form_dropdown('id_lugar_ocurre', $lugares, $id_lugar_ocurre);?></td>
                </tr>
			</table>
		</td>
	</tr>

    <tr>
        <!--<td><strong>Línea de intervención</strong></td> -->
		<td rowspan="2"><strong>Línea de intervención</strong></td>
        <td>
            <?php echo form_dropdown('linea_id_llam', $lineas, $linea_id_llam, 'onchange="activar_entidades();"');?><br/>
            Cual entidad:
            <?php 
            $activo = ($entidad =="") ? 'disabled="disabled"': "";
            echo form_dropdown('entidad', $entidades, $entidad, $activo);
            ?>
            Otra entidad:
            <?php 
            $activo = ($otra_entidad =="") ? array('disabled'=>"disabled") : array();
            echo form_input(array('name'=>'otra_entidad','value'=>$otra_entidad,'size'=>"20", 'maxlength'=>"255") + $activo);
            ?>
        </td>
		
		<tr>
		  <td style="height: 23px">
		    Respuestas Efectivas
			<?php 
			$activo = ($respuesta_llamada_id=="") ? 'disabled="disabled"': "";
            echo form_dropdown('respuesta_llamada_id', $respuesta, $respuesta_llamada_id, $activo);
			?>
		  </td>
        </tr>
    </tr>
	
	
	
    <tr>
        <td><strong>Consulta aseguramiento</strong></td>
        <td><a href="http://app.saludcapital.gov.co/comprobadordederechos/" target="_blank">Comprobador de Derechos</a> </td>
    </tr>

	<tr>
        <td><strong>Consulta Sivigila</strong></td>
        <td><a href="http://appa.saludcapital.gov.co/SivigilaDC" target="_blank">Sivigila</a> </td>
    </tr>
	
    <tr>
        <td><strong>Intervención</strong></td>
        <td><?php echo form_textarea(array('name'=>'intervencion', 'value'=>$intervencion,'cols'=>'80', 'rows'=>'10'));?></td>
    </tr>
</table>
<br/>    
<table>
    <tr>
        <th colspan="2">CIERRE</th>
    </tr>
    <tr>
        <td><strong>Observaciones</strong></td>
        <td><?php echo form_textarea(array('name'=>'observaciones', 'value'=>$observaciones,'cols'=>'80', 'rows'=>'10'));?></td>
    </tr>
    <tr>
        <td><strong>Hora de cierre</strong></td>
        <td><input type="text" name="hora_cierre" value="<?php echo $hora;?>"  size="5" maxlength="5" /></td>
    </tr>
    <tr>
        <td class="blanco" colspan="2" align="center">
            <input type="submit" value="Guardar" />
        </td>
    </tr>
</table>

<?php echo form_close('<br/><br/>');?>