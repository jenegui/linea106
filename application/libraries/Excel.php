<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Excel library for Code Igniter applications
* Author: Derek Allard, Dark Horse Consulting, www.darkhorse.to, April 2006
*/

class Excel {

    function export($query, $filename='exceloutput')
    {
         $headers = ''; // just creating the var for field headers to append to below
         $data = ''; // just creating the var for field data to append to below

         $fields = $query->first_row('array');
         if ($query->num_rows() == 0) {
              echo '<p>The table appears to have no data.</p>';
         } else {
             foreach ($fields as $key => $value) {
                 $headers .= $key . "\t";
              }

              foreach ($query->result() as $row) {
                   $line = '';
                   foreach($row as $value) {
                        if ((!isset($value)) OR ($value == "")) {
                             $value = "\t";
                        } else {
                             $value = str_replace('"', '""', $value);
                             $value = '"' . $value . '"' . "\t";
                             $value = mb_convert_encoding($value, 'ISO-8859-15', 'UTF-8');
                        }
                        $line .= $value;
                   }
                   $data .= trim($line)."\n";
              }

              $data = str_replace("\r","",$data);

              header("Content-type: application/x-msdownload");
              header("Content-Disposition: attachment; filename=$filename.xls");
              echo "$headers\n$data";
         }
    }
}
/* EOF */