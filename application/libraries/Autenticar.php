<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Autentica el tipo de usuario
 */


class Autenticar {

    /*function Autenticar()
    {
        $this->CI =& get_instance();
    }*/

    function __construct() {
        $this->CI =& get_instance();
    }

    function tipo_usuario($tipo_requerido)
    {
        if ($this->CI->session->userdata('tipo_usuario') != $tipo_requerido) {
            redirect('ingreso/error');
        }
    }

    function usuario()
    {
        if ($this->CI->session->userdata('usuario') == "") {
            redirect('ingreso/error');
        }
    }
}

?>
