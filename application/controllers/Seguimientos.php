<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seguimientos extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        //$this->autenticar->usuario();
        $this->load->view('autenticar/encabezado');
        $this->load->view('autenticar/menu');
    }

    //Buscar y seleccionar el caso
    function index()
    {
        //PREPARAR CAMPOS
        $data['num_doc'] = $this->input->post('num_doc');
        $data['nombre'] = mb_strtoupper(strtok($this->input->post('nombre'), ' '), 'UTF-8');
        $data['apellido'] = mb_strtoupper(strtok($this->input->post('apellido'), ' '), 'UTF-8');
        $data['profesional'] = ($this->input->post('profesional') =='') ? $this->session->usuario : $this->input->post('profesional');
        $data['entidad'] = $this->input->post('entidad');
        $data['inicio'] = ($this->input->post('inicio') =='') ? date("Y-m-d", mktime(0, 0, 0, date("m")-1 , 1, date("Y"))) : $this->input->post('inicio');
        $data['final'] = ($this->input->post('final') =='') ? date("Y-m-d") : $this->input->post('final');
        //BUSCAR
        if ($this->input->post('ok') =="Buscar") {
            if ($data['num_doc'] !="") {$this->db->where('num_doc', $data['num_doc']);} 
            if ($data['nombre'] !="") {$this->db->like('primer_nombre', $data['nombre']);} 
            if ($data['apellido'] !="") {$this->db->like('primer_apellido', $data['apellido']);} 
            if ($data['profesional'] !="") {$this->db->like('profesional', $data['profesional']);} 
            if ($data['entidad'] !="") {$this->db->like('entidad', $data['entidad']);} 
            $this->db->where('hora_inicio >', $data['inicio'].' 00:00:00');
            $this->db->where('hora_inicio <', $data['final'].' 23:59:59');
            $this->db->join('motivos', 'motivo_id = motivo_id_llam');
            $this->db->join('lineas', 'linea_id = linea_id_llam');
            $this->db->order_by('llamada_id', 'desc');
            $data['llamadas'] = $this->db->get('llamadas');
        }
        //Combo Usuarios
        $this->db->where('clave IS NOT NULL');
        $this->db->order_by('nombre_usuario');
        $query = $this->db->get('usuarios');
        foreach ($query->result() as $row) {
            $data['usuarios'][$row->usuario] = $row->nombre_usuario.' '.$row->apellido_usuario;
        }
        //Combo Entidades
        $data['entidades'][''] = "-";
        $this->db->order_by('tipo_entidad');
        $query = $this->db->get("entidades");
        foreach ($query->result() as $row) {
            $data['entidades'][$row->entidad] = $row->tipo_entidad.': '.$row->entidad;
        }
        //Cargar Vista
        $this->load->view('seg_buscador', $data);
        $this->load->view('pie');
    }

    function capturar()
    {
        if ($this->input->post('llamada_id') >0) {
            $llamada_id = $this->input->post('llamada_id');
        } elseif ((int)$this->uri->segment(3) >0 ) {
            $llamada_id = (int)$this->uri->segment(3);
        } else {
            $llamada_id =0;
        }
        //Recuperar datos de la llamada
        if ($llamada_id >0) {

            //BUSCAR LA LLAMADA
            $this->db->join('localidades', 'localidad_id = localidad_id_llam', 'left');
            $this->db->join('comos', 'como_id = como_id_llam', 'left');
            $this->db->join('etnias', 'etnia_id = etnia_id_llam', 'left');
            $this->db->join('motivos', 'motivo_id = motivo_id_llam');
            $this->db->join('lineas', 'linea_id = linea_id_llam');
            $this->db->where('llamada_id', $llamada_id);
            $query = $this->db->get('llamadas');

            $data['llamada'] = $query->first_row('array');

            //Recuperar datos de seguimiento
            $this->db->where('llamada_id_seg', $llamada_id);
            $this->db->order_by('fecha_seg');
            $data['seguimientos'] = $this->db->get('seguimientos');

            //Combos y campos
            $data['medios'] = array(''=>'', 'Oficio'=>'Oficio', 'Correo electrónico'=>'Correo electrónico', 'Llamada'=>'Llamada');
            $data['quienes'] = array(''=>'', 'Institución'=>'Institución', 'Comunidad'=>'Comunidad', 'Usuario'=>'Usuario');
            $data['estados'] = array(''=>'', 'Abierto'=>'Abierto', 'Cerrado'=>'Cerrado', 'Alerta'=>'Alerta');

            //Cargar la vista
            $this->load->view('seg_capturar', $data);
            $this->load->view('pie');
        }
    }

    function guardar()
    {
        if ($this->input->post('llamada_id') >0) {
            //Armar consulta a insertar
            $seg['llamada_id_seg'] = $this->input->post('llamada_id');
            $seg['fecha_seg'] = $this->input->post('fecha_seg');
            $seg['fecha_seg'] = $this->input->post('fecha_seg');
            $seg['medio'] = $this->input->post('medio');
            $seg['quien'] = $this->input->post('quien');
            $seg['descripcion_quien'] = $this->input->post('descripcion_quien');
            $seg['observaciones'] = $this->input->post('observaciones');
            $seg['estado'] = $this->input->post('estado');
            $seg['registrado_seg'] = date("Y-m-d H:i:s");
            $seg['profesional_seg'] = $this->session->userdata('usuario');
            //Insertar en la base de datos
            $this->db->insert('seguimientos', $seg);
            //Regresar a la pantalla de registro
            redirect('seguimientos/capturar/'.$seg['llamada_id_seg']);
        }
    }
}