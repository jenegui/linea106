<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        //$this->autenticar->usuario();
        $this->load->model('Reportes_model');
        $this->load->view('autenticar/encabezado');
        $this->load->view('autenticar/menu');
    }

//CONTROLADORES

    //seleccion del reporte
    function index()
    {
        $data['mensaje'] = "";
        $data['inicio'] = date("Y-m-d", mktime(0, 0, 0, date("m")-1 , 1, date("Y")));
        $data['final'] =  date("Y-m-d", mktime(0, 0, 0, date("m") , 0, date("Y")));
        $this->load->view('seleccion_reporte', $data);
        $this->load->view('pie');

    }

    //Valida los datos de seleccion del reporte y redirecciona al seleccionado
    function validar()
    {
        $data = $_POST;
        $data['mensaje'] = "";
        $base = strtotime('2010-12-01');

        @list($year, $month, $day) = explode('-', $data['inicio']);
        if (@!checkdate($month, $day, $year)) {
            $data['mensaje'] .= "ERROR: La fecha de inicio no es válida!<br/>\n";
        } elseif (strtotime($data['inicio']) > time() ) {
            $data['mensaje'] .= "ERROR: La fecha de inicio es futura!<br/>\n";
        } elseif (strtotime($data['inicio']) < $base ) {
            $data['mensaje'] .= "ERROR: La fecha de inicio es antes de la base de datos!<br/>\n";
        }

        @list($year, $month, $day) = explode('-', $data['final']);
        if (@!checkdate($month, $day, $year)) {
            $data['mensaje'] .= "ERROR: La fecha final no es válida!<br/>\n";
        } elseif (strtotime($data['final']) > time() ) {
            $data['mensaje'] .= "ERROR: La fecha final es futura!<br/>\n";
        } elseif (strtotime($data['final']) < $base ) {
            $data['mensaje'] .= "ERROR: La fecha final es antes de la base de datos!<br/>\n";
        } elseif (strtotime($data['final']) < strtotime($data['inicio']) ) {
            $data['mensaje'] .= "ERROR: La fecha final es antes de la fecha de inicio!<br/>\n";
        }

        if (!isset($data['tipo']))
            $data['mensaje'] .= "ERROR: Falta seleccionar el reporte!<br/>\n";

        if ($data['mensaje']) {
            $this->load->view('seleccion_reporte', $data);
            $this->load->view('pie');
        } else {
            $this->session->set_userdata('inicio', $data['inicio']);
            $this->session->set_userdata('final', $data['final']);
            if ($data['tipo'] == "poblacion"){
                redirect('reportes/poblacion/'.$data['poblacion']);
            } else {
                redirect('reportes/'.$data['tipo']);
            }
        }
    }

    //Reporte por línea de intervención
    function linea()
    {
        $data['query'] = $this->Reportes_model->general('linea', 'lineas');
        $data['titulo'] = "Reporte por Línea de Intervención";
        $data['columna'] = "Línea";

        $this->load->view('reporte_4col', $data);
        $this->load->view('pie');

    }

    //Reporte por motivo de consulta
    function motivo()
    {
        $data['query'] = $this->Reportes_model->general('motivo', 'motivos');
        $data['titulo'] = "Reporte por Motivo de Consulta";
        $data['columna'] = "Motivo";

        $this->load->view('reporte_4col', $data);
        $this->load->view('pie');

    }

    //Reporte por motivo de consulta agrupado
    function agrupado()
    {
        $data['query'] = $this->Reportes_model->general('motivo', 'grupos');
        $data['titulo'] = "Reporte por Motivo de Consulta Agrupado";
        $data['columna'] = "Grupo de Motivos";

        $this->load->view('reporte_4col', $data);
        $this->load->view('pie');

    }

    //Reporte por localidad
    function localidad()
    {
        $data['query'] = $this->Reportes_model->general('localidad', 'localidades');
        $data['titulo'] = "Reporte por Localidad";
        $data['columna'] = "Localidad";

        $this->load->view('reporte_4col', $data);
        $this->load->view('pie');

    }

    //Reporte de llamadas por profesional
    function llamadas()
    {
        $this->db->select('CONCAT(nombre_usuario," ",apellido_usuario) AS concepto', FALSE);
        $this->db->select('COUNT(*) AS total');
        $this->db->join('usuarios', 'profesional = usuario');
        $this->db->where('hora_inicio >', $this->session->userdata('inicio').' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->userdata('final').' 23:59:59');
        $this->db->group_by('profesional');
        $data['query'] = $this->db->get('llamadas');

        $data['titulo'] = "Reporte de Intervenciones por Profesional";
        $data['columna'] = "Nombre";

        $this->load->view('reporte_2col', $data);
        $this->load->view('pie');

    }

    //Reporte de motivos por cada profesional
    function motivos_profesional()
    {
        $data['query'] = $this->Reportes_model->profesional('motivo');
        $data['titulo'] = "Reporte de Motivos de Consulta para <i>" .$this->session->userdata('usuario')."</i>";
        $data['columna'] = "Motivo";

        $this->load->view('reporte_2col', $data);
        $this->load->view('pie');
    }

    //Reporte de motivos por cada profesional
    function lineas_profesional()
    {
        $data['query'] = $this->Reportes_model->profesional('linea');
        $data['titulo'] = "Reporte de Líneas de Intervención para <i>" .$this->session->userdata('usuario')."</i>";
        $data['columna'] = "Línea";

        $this->load->view('reporte_2col', $data);
        $this->load->view('pie');
    }

    //Reporte por entidad
    function entidad()
    {
        $data['query'] = $this->Reportes_model->entidades();
        $data['titulo'] = "Reporte por Entidad";
        $data['columna'] = "Entidad";

        $this->load->view('reporte_2col', $data);
        $this->load->view('pie');

    }

    //Reporte por tipo de poblacion
    function poblacion()
    {
        $data['query'] = $this->Reportes_model->poblacion($this->uri->segment(3));
		$poblacion = ($this->uri->segment(3) =='lgbt') ? 'LGBTI': $this->uri->segment(3);
        $data['titulo'] = "Reporte para población <i>" .$poblacion."</i>";
        $data['columna'] = "Motivo";

        $this->load->view('reporte_4col', $data);
        $this->load->view('pie');
    }
	
	//Reporte total del periodo
    function exportar()
    {
        //cargar libreria
        $this->load->library('Excel');
        //consulta
        $this->db->select(' llamada_id AS id_llamada, hora_inicio, hora_cierre, hora_registro, origen, telefono, antiguo,
                            tipo_doc_id_llam AS tipo_documento, num_doc AS Documento,
                            primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, edad, sexo,
                            loc1.localidad AS localidad, colegio_jardin, sede, grado,
                            loc2.localidad AS loc_colegio,
                            gestante, desplazado, hab_calle, discapacidad, lgbt AS LGBTI, trabajador_infantil,
                            no_escolarizado, conflicto, ive, navidad, etnia, motivo, linea, entidad, 
							respuesta_llamada_id As Respuesta Efectiva,
                            tema_semana, como, observaciones, profesional');
							
		//datos casos de violencia
		$this->db->select(' id_violencia As Id_Caso Violencia, primer_nombre_victima, segundo_nombre_victima, 
							primer_apellido_victima, segundo_apellido_victima, edad_victima, direccion_victima, 
							telefono_victima, primer_nombre_agresor, segundo_nombre_agresor, primer_apellido_agresor, 
							segundo_apellido_agresor, edad_agresor, direccion_agresor, telefono_agresor, 
							relacion AS Relacion Agresor-Victima, lugar AS Lugar Ocurrencia                         
						 ');  
		
        $this->db->join('motivos', 'motivo_id = motivo_id_llam');
        $this->db->join('lineas', 'linea_id = linea_id_llam');
        $this->db->join('localidades AS loc1', 'loc1.localidad_id = localidad_id_llam', 'left');
        $this->db->join('localidades AS loc2', 'loc2.localidad_id = localidad_colegio', 'left');
        $this->db->join('etnias', 'etnia_id = etnia_id_llam', 'left');
        $this->db->join('comos', 'como_id = como_id_llam', 'left');
		$this->db->join('violencias', 'id_llamada_vio =	llamada_id', 'left');
		$this->db->join('lugares', 'id_lugar = id_lugar_ocurre', 'left');
        $this->db->join('relaciones', 'id_relacion = id_relacion_agresor_victima', 'left');
        $this->db->where('hora_inicio >', $this->session->userdata('inicio').' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->userdata('final').' 23:59:59');
        $this->db->order_by("llamada_id", "desc");
        $query = $this->db->get('llamadas');
        //Exportar
        $nom_archivo = 'linea106_de_'.$this->session->userdata('inicio').
                '_a_'.$this->session->userdata('final');
        $this->excel->export($query, $nom_archivo);
        //Salir para no enviar la vista de encabezado
        exit;
    }
    
	//Reporte total del periodo para profesionales: resumido
	function exportar_resumen()
    {
        //cargar libreria
        $this->load->library('Excel');
        //consulta
        $this->db->select(' llamada_id AS Id, hora_inicio, hora_cierre, hora_registro, origen, Linea, 
							Motivo, antiguo AS Tipo Usuario, loc1.localidad AS Localidad, Edad, Sexo, 
							colegio_jardin, Sede, Grado, loc2.localidad AS Loc_Colegio, 
							Gestante, Desplazado, Hab_calle, Discapacidad, lgbt AS LGBTI, Trabajador_infantil,
                            no_escolarizado, Conflicto, como, Entidad, respuesta_llamada_id As Respuesta Efectiva, 
							Observaciones, Profesional');
		
		$this->db->join('motivos', 'motivo_id = motivo_id_llam');
        $this->db->join('lineas', 'linea_id = linea_id_llam');
        $this->db->join('localidades AS loc1', 'loc1.localidad_id = localidad_id_llam', 'left');
        $this->db->join('localidades AS loc2', 'loc2.localidad_id = localidad_colegio', 'left');
        $this->db->join('etnias', 'etnia_id = etnia_id_llam', 'left');
        $this->db->join('comos', 'como_id = como_id_llam', 'left');
		$this->db->join('violencias', 'id_llamada_vio =	llamada_id', 'left');
		$this->db->join('lugares', 'id_lugar = id_lugar_ocurre', 'left');
        $this->db->join('relaciones', 'id_relacion = id_relacion_agresor_victima', 'left');
        $this->db->where('hora_inicio >', $this->session->userdata('inicio').' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->userdata('final').' 23:59:59');
        $this->db->order_by("llamada_id", "desc");
		$query = $this->db->get('llamadas');
		
        //Exportar
        $nom_archivo = 'linea106_de_'.$this->session->userdata('inicio').
                '_a_'.$this->session->userdata('final');
        $this->excel->export($query, $nom_archivo);
        //Salir para no enviar la vista de encabezado
        exit;
    }
	
	
    function exportar_violencia()
    {
        //cargar libreria
        $this->load->library('Excel');
        //consulta
        $this->db->select(' primer_nombre_victima, segundo_nombre_victima, primer_apellido_victima, segundo_apellido_victima,
                            edad_victima, direccion_victima, telefono_victima, 
                            primer_nombre_agresor, segundo_nombre_agresor, primer_apellido_agresor, segundo_apellido_agresor,
                            edad_agresor, direccion_agresor, telefono_agresor,
                            relacion AS relacion_agresor_victima,
                            lugar AS lugar_ocurrencia');  
        $this->db->select(' llamada_id AS id, hora_inicio, hora_cierre, hora_registro, origen, telefono, antiguo,
                            tipo_doc_id_llam AS tipo_documento, num_doc AS documento_identidad,
                            primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, edad, sexo,
                            loc1.localidad AS localidad, colegio_jardin, sede, grado,
                            loc2.localidad AS loc_colegio,
                            gestante, desplazado, hab_calle, discapacidad, lgbt AS lgbti, trabajador_infantil,
                            no_escolarizado, conflicto, ive, navidad, etnia, motivo, narrativa, linea, entidad, 
                            tema_semana, intervencion, como, observaciones, profesional');
                          
        $this->db->join('motivos', 'motivo_id = motivo_id_llam');
        $this->db->join('lineas', 'linea_id = linea_id_llam');
        $this->db->join('localidades AS loc1', 'loc1.localidad_id = localidad_id_llam', 'left');
        $this->db->join('localidades AS loc2', 'loc2.localidad_id = localidad_colegio', 'left');
        $this->db->join('etnias', 'etnia_id = etnia_id_llam', 'left');
        $this->db->join('comos', 'como_id = como_id_llam', 'left');
        $this->db->join('violencias', 'llamada_id = id_llamada_vio');
        $this->db->join('lugares', 'id_lugar = id_lugar_ocurre', 'left');
        $this->db->join('relaciones', 'id_relacion = id_relacion_agresor_victima', 'left');
        
        $this->db->where('hora_inicio >', $this->session->userdata('inicio').' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->userdata('final').' 23:59:59');
        if ($this->session->tipo_usuario != 'administrador') {
            $this->db->where('profesional', $this->session->usuario);
        }
        $this->db->order_by("llamada_id", "desc");
        $query = $this->db->get('llamadas');
        //Exportar
        $nom_archivo = 'casos_violencia_de_'.$this->session->userdata('inicio').
                '_a_'.$this->session->userdata('final');
        $this->excel->export($query, $nom_archivo);
        //Salir para no enviar la vista de encabezado
        exit;
    }
	
	//Reporte sseguimiento a las atenciones
    function exportar_seg()
    {
        //cargar libreria
        $this->load->library('Excel');
        //consulta
        $this->db->select(' llamada_id AS llamada, hora_registro AS registro_llamada,
                            profesional AS profesional_llamada, origen, telefono,
                            tipo_doc_id_llam AS tipo_documento, num_doc AS documento_identidad,
                            primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, edad, sexo,
                            loc1.localidad AS localidad, motivo AS motivo_llamada,
                            seg_id AS seguimiento, fecha_seg AS fecha_seguimiento,
                            medio, quien, descripcion_quien, observaciones, estado,
                            registrado_seg AS registro_seguimiento, profesional_seg AS profesional_seguimiento,');
        $this->db->join('llamadas', 'llamada_id = llamada_id_seg');
        $this->db->join('motivos', 'motivo_id = motivo_id_llam');
        $this->db->join('localidades AS loc1', 'loc1.localidad_id = localidad_id_llam', 'left');
        $this->db->where('registrado_seg >', $this->session->userdata('inicio').' 00:00:00');
        $this->db->where('registrado_seg <', $this->session->userdata('final').' 23:59:59');
        if ($this->session->tipo_usuario != 'administrador') {
            $this->db->where('profesional', $this->session->usuario);
        }
        $this->db->order_by("llamada_id, seg_id");
        $query = $this->db->get('seguimientos');
        //Exportar
        $nom_archivo = 'seguimientos_de_'.$this->session->userdata('inicio').
                '_a_'.$this->session->userdata('final');
        $this->excel->export($query, $nom_archivo);
        //Salir para no enviar la vista de encabezado
        exit;
    }
	
	//Reporte monitoreo de las atenciones 2016-12-28
	function exportar_monitoreo()
    {
        //cargar libreria
        $this->load->library('Excel');
        //consulta
        $this->db->select(' llamada_id AS Id Llamada, hora_registro AS registro_llamada, origen, primer_nombre AS Primer Nombre, segundo_nombre AS Segundo Nombre, 
		                    primer_apellido AS Primer Apellido, segundo_apellido AS Segundo Apellido, telefono, edad AS Edad, 
							sexo AS Sexo, loc1.localidad, colegio_jardin, loc2.localidad AS Loc_Colegio, discapacidad, lgbt AS LGBTI, 
							ive, motivo, Linea, narrativa, intervencion, Entidad, respuesta_llamada_id As Respuesta Efectiva, tema_semana,
                            Observaciones, Profesional');
        		
		$this->db->join('motivos', 'motivo_id = motivo_id_llam');
        $this->db->join('lineas', 'linea_id = linea_id_llam');
        $this->db->join('localidades AS loc1', 'loc1.localidad_id = localidad_id_llam', 'left');
        $this->db->join('localidades AS loc2', 'loc2.localidad_id = localidad_colegio', 'left');
        $this->db->join('etnias', 'etnia_id = etnia_id_llam', 'left');
        $this->db->join('comos', 'como_id = como_id_llam', 'left');
		$this->db->join('violencias', 'id_llamada_vio =	llamada_id', 'left');
		$this->db->join('lugares', 'id_lugar = id_lugar_ocurre', 'left');
        $this->db->join('relaciones', 'id_relacion = id_relacion_agresor_victima', 'left');
        $this->db->where('hora_inicio >', $this->session->userdata('inicio').' 00:00:00');
        $this->db->where('hora_inicio <', $this->session->userdata('final').' 23:59:59');
        $this->db->order_by("llamada_id", "desc");
		$query = $this->db->get('llamadas');
		
        //Exportar
        $nom_archivo = 'linea106_de_'.$this->session->userdata('inicio').
                '_a_'.$this->session->userdata('final');
        $this->excel->export($query, $nom_archivo);
        //Salir para no enviar la vista de encabezado
        exit;
    }
	
}

/*EOF*/
