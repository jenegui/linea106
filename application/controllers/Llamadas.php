<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Llamadas extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        //$this->autenticar->usuario();
        
        $this->load->model('Busquedas');
        $this->load->view('autenticar/encabezado');
        $this->load->view('autenticar/menu');
    }

    function index()
    {
        //Preparar campos
        $data = $this->Busquedas->campos(); 
        //Valores por defecto
        $data += array(
            'origen'=>'',
            'telefono'=>'',
            'antiguo'=>'',
            'tipo_doc_id_llam'=>'MS',
            'num_doc'=>'',
            'primer_nombre'=>'',
            'segundo_nombre'=>'',
            'primer_apellido'=>'',
            'segundo_apellido'=>'',
            'nombre_identitario'=>'',
            'primer_nombre_agresor'=>'',
            'quien_otro'=>'',
            'tipo_doc_id_llam_otro'=>'AS',
            'num_doc_otro'=>'',
            'primer_nombre_otro'=>'',
            'segundo_nombre_otro'=>'',
            'primer_apellido_otro'=>'',
            'segundo_apellido_otro'=>'',
            'primer_nombre_agresor'=>'',
            'segundo_nombre_agresor'=>'',
            'primer_apellido_agresor'=>'',
            'segundo_apellido_agresor'=>'',
            'primer_nombre_victima'=>'',
            'segundo_nombre_victima'=>'',
            'primer_apellido_victima'=>'',
            'segundo_apellido_victima'=>'',
            'fecha_nacimiento'=>'',
            'd_N'=>'',
            'm_N'=>'',
            'a_N'=>'',
            'edad'=>'',
            'edad_otro'=>'',
            'edad_agresor'=>'',
            'edad_victima'=>'',
            'sexo_agresor'=>'',
            'sexo'=>'',
            'genero'=>'',
            'orientacion'=>'',
            'sexo_otro'=>'',
            'sexo_victima'=>'',
            'localidad_id_llam'=>'',
            'como_id_llam'=>'',
            'colegio_jardin'=>'',
            'sede'=>'',
            'grado'=>'',
            'localidad_colegio'=>'',
            'id_jornada_llam'=>'',
            'id_tipo_edu_llam'=>'',
            'gestante'=>'',
            'desplazado'=>'',
            'hab_calle'=>'',
            'priv_libertad'=>'',
            'inmigrante'=>'',
            'reincorporacion'=>'',
            'discapacidad'=>'',
            'tdah'=>'',
            'lgbt'=>'',
            'trabajador_infantil'=>'',
            'no_escolarizado'=>'',
            'conflicto'=>'',
            'ive'=>'',
            'navidad'=>'',
            'etnia_id_llam'=>'',
            'motivo_id_llam'=>'',
            'motivo_2'=>'',
            'motivo_3'=>'',
            'narrativa'=>'',
            'linea_id_llam'=>'',
            'entidad'=>'',
            'otra_entidad'=>'',
            'tema_semana'=>'',
            'intervencion'=>'',
            'observaciones'=>'',
            'direccion_agresor'=>'',
            'direccion_victima'=>'',
            'telefono_agresor'=>'',
            'telefono_victima'=>'',
            'barrio_victima'=>'',
            'localidad_victima'=>'',
            'id_relacion_agresor_victima'=>'',
            'id_lugar_ocurre'=>'',
			'respuesta_llamada_id'=>'',
        );
        //Vista
        $this->load->view('nueva_llamada', $data);
        $this->load->view('pie');
    }
    
    function mismo_caso()
    {
        //Preparar campos
        $data = $this->Busquedas->campos(); 
        
        //Recuperar valores de la anterior llamada
        $this->db->where('llamada_id', $this->input->post('llamada_id'));
        $this->db->join('violencias','id_llamada_vio =  llamada_id','left');
        $query = $this->db->get('llamadas');
        $data += $query->row_array(); 
        $data['antiguo'] = '1';
        //Busca si la entidad esta en la lista
        $this->db->where('entidad', $data['entidad']);
        $query = $this->db->get('entidades');
        $data['otra_entidad'] = ($query->num_rows() == 0) ? $data['entidad'] : "";
        //Vista
        $this->load->view('nueva_llamada', $data);
        $this->load->view('pie');
    }

    //GUARDA LOS DATOS DE LA LLAMADA
    function guardar()
    {
        //RECUPERA LOS DATOS OBLIGATORIOS
        $datos['hora_inicio'] = $this->input->post('hora_inicio');
        $datos['origen'] = $this->input->post('origen');
        $datos['telefono'] = $this->input->post('telefono');
        $datos['tipo_doc_id_llam'] = $this->input->post('tipo_doc_id_llam');
        $datos['sexo'] = $this->input->post('sexo');
        $datos['edad'] = $this->input->post('edad');
        $datos['motivo_id_llam'] = $this->input->post('motivo_id_llam');
        $datos['linea_id_llam'] = $this->input->post('linea_id_llam');
        $datos['hora_cierre'] = $this->input->post('hora_cierre');
        //RECUPERA LOS DATOS ADICIONALES
        if ($this->input->post('antiguo') !='') $datos['antiguo'] = $this->input->post('antiguo');
        if ($this->input->post('num_doc') !='') $datos['num_doc'] = $this->input->post('num_doc');
        if ($this->input->post('regimen_salud') !='') $datos['regimen_salud'] = $this->input->post('regimen_salud');
        if ($this->input->post('genero') !='') $datos['genero'] = $this->input->post('genero');
        if ($this->input->post('orientacion') !='') $datos['orientacion'] = $this->input->post('orientacion');
        if ($this->input->post('num_doc_otro') !='') {
            $datos['tipo_doc_id_llam_otro'] = $this->input->post('tipo_doc_id_llam_otro');
            $datos['num_doc_otro'] = $this->input->post('num_doc_otro');
        }
        if ($this->input->post('edad_otro') !='') $datos['edad_otro'] = $this->input->post('edad_otro');
        if ($this->input->post('sexo_otro') !='') $datos['sexo_otro'] = $this->input->post('sexo_otro');
        if ($this->input->post('quien_otro') !='') $datos['quien_otro'] = $this->input->post('quien_otro');
        
        if ($this->input->post('localidad_id_llam') !='') $datos['localidad_id_llam'] = $this->input->post('localidad_id_llam');
        if ($this->input->post('como_id_llam') !='') $datos['como_id_llam'] = $this->input->post('como_id_llam');
        if ($this->input->post('colegio_jardin') !='') $datos['colegio_jardin'] = $this->input->post('colegio_jardin');
        if ($this->input->post('sede') !='') $datos['sede'] = $this->input->post('sede');
        if ($this->input->post('grado') !='') $datos['grado'] = $this->input->post('grado');
        if ($this->input->post('localidad_colegio') !='') $datos['localidad_colegio'] = $this->input->post('localidad_colegio');
        if ($this->input->post('id_jornada_llam') !='') $datos['id_jornada_llam'] = $this->input->post('id_jornada_llam');
        if ($this->input->post('id_tipo_edu_llam') !='') $datos['id_tipo_edu_llam'] = $this->input->post('id_tipo_edu_llam');
        if ($this->input->post('motivo_2') !='') $datos['motivo_2'] = $this->input->post('motivo_2');
        if ($this->input->post('motivo_3') !='') $datos['motivo_3'] = $this->input->post('motivo_3');
        if ($this->input->post('narrativa') !='') $datos['narrativa'] = $this->input->post('narrativa');
        if ($this->input->post('entidad') !='') $datos['entidad'] = $this->input->post('entidad');
        if ($this->input->post('otra_entidad') !='') $datos['otra_entidad'] = $this->input->post('otra_entidad');
        if ($this->input->post('tema_semana') !='') $datos['tema_semana'] = $this->input->post('tema_semana');
        if ($this->input->post('intervencion') !='') $datos['intervencion'] = $this->input->post('intervencion');
        if ($this->input->post('observaciones') !='') $datos['observaciones'] = $this->input->post('observaciones');
		if ($this->input->post('respuesta_llamada_id') !='') $datos['respuesta_llamada_id'] = $this->input->post('respuesta_llamada_id');
		
        //RECUPERA CHECKBOXES
         if ($this->input->post('hab_calle') !='') $datos['hab_calle'] = $this->input->post('hab_calle');
         if ($this->input->post('priv_libertad') !='') $datos['priv_libertad'] = $this->input->post('priv_libertad');
         if ($this->input->post('inmigrante') !='') $datos['inmigrante'] = $this->input->post('inmigrante');
         if ($this->input->post('discapacidad') !='') $datos['discapacidad'] = $this->input->post('discapacidad');
         if ($this->input->post('conflicto') !='') $datos['conflicto'] = $this->input->post('conflicto');
         if ($this->input->post('reincorporacion') !='') $datos['reincorporacion'] = $this->input->post('reincorporacion');         
        if ($this->input->post('gestante') !='') $datos['gestante'] = $this->input->post('gestante');

        if ($this->input->post('tipo_discapacidad') !='') $datos['tipo_discapacidad'] = $this->input->post('tipo_discapacidad');

        
        //RECUPERA FECHA DE NACIMIENTO
        if (@checkdate($this->input->post('m_N'), $this->input->post('d_N'), $this->input->post('a_N'))) {
            $datos['fecha_nacimiento'] = $this->input->post('a_N').'-'.$this->input->post('m_N').'-'.$this->input->post('d_N');
        }
        //RECUPERA DATOS CASO VIOLENCIA
        if ($this->input->post('id_lugar_ocurre') !='') $caso['id_lugar_ocurre'] = $this->input->post('id_lugar_ocurre');
        if ($this->input->post('id_relacion_agresor_victima') !='') $caso['id_relacion_agresor_victima'] = $this->input->post('id_relacion_agresor_victima');
        if ($this->input->post('edad_agresor') !='') $caso['edad_agresor'] = $this->input->post('edad_agresor');
        if ($this->input->post('edad_victima') !='') $caso['edad_victima'] = $this->input->post('edad_victima');
        if ($this->input->post('sexo_agresor') !='') $caso['sexo_agresor'] = $this->input->post('sexo_agresor');
        if ($this->input->post('sexo_victima') !='') $caso['sexo_victima'] = $this->input->post('sexo_victima');
        if ($this->input->post('direccion_agresor') !='') $caso['direccion_agresor'] = $this->input->post('direccion_agresor');
        if ($this->input->post('telefono_agresor') !='') $caso['telefono_agresor'] = $this->input->post('telefono_agresor');
        if ($this->input->post('direccion_victima') !='') $caso['direccion_victima'] = $this->input->post('direccion_victima');
        if ($this->input->post('telefono_victima') !='') $caso['telefono_victima'] = $this->input->post('telefono_victima');
        if ($this->input->post('barrio_victima') !='') $caso['barrio_victima'] = $this->input->post('barrio_victima');
        if ($this->input->post('localidad_victima') !='') $caso['localidad_victima'] = $this->input->post('localidad_victima');
        
        //AJUSTES
        $datos['hora_inicio'] =  date("Y-m-d")." ".$datos['hora_inicio'].":00" ;
        $datos['hora_cierre'] =  date("Y-m-d")." ".$datos['hora_cierre'].":00" ;
        $datos['hora_registro'] = date("Y-m-d H:i:s");

        if ($this->input->post('primer_nombre') !='') $datos['primer_nombre'] = mb_strtoupper($this->input->post('primer_nombre'), 'UTF-8');
        if ($this->input->post('segundo_nombre') !='') $datos['segundo_nombre'] = mb_strtoupper($this->input->post('segundo_nombre'), 'UTF-8');
        if ($this->input->post('primer_apellido') !='') $datos['primer_apellido'] = mb_strtoupper($this->input->post('primer_apellido'), 'UTF-8');
        if ($this->input->post('segundo_apellido') !='') $datos['segundo_apellido'] = mb_strtoupper($this->input->post('segundo_apellido'), 'UTF-8');
        if ($this->input->post('nombre_identitario') !='') $datos['nombre_identitario'] = mb_strtoupper($this->input->post('nombre_identitario'), 'UTF-8');
        
        if ($this->input->post('primer_nombre_otro') !='') $datos['primer_nombre_otro'] = mb_strtoupper($this->input->post('primer_nombre_otro'), 'UTF-8');
        if ($this->input->post('segundo_nombre_otro') !='') $datos['segundo_nombre_otro'] = mb_strtoupper($this->input->post('segundo_nombre_otro'), 'UTF-8');
        if ($this->input->post('primer_apellido_otro') !='') $datos['primer_apellido_otro'] = mb_strtoupper($this->input->post('primer_apellido_otro'), 'UTF-8');
        if ($this->input->post('segundo_apellido_otro') !='') $datos['segundo_apellido_otro'] = mb_strtoupper($this->input->post('segundo_apellido_otro'), 'UTF-8');
        
        if ($this->input->post('primer_nombre_agresor') !='') $caso['primer_nombre_agresor'] = mb_strtoupper($this->input->post('primer_nombre_agresor'), 'UTF-8');
        if ($this->input->post('segundo_nombre_agresor') !='') $caso['segundo_nombre_agresor'] = mb_strtoupper($this->input->post('segundo_nombre_agresor'), 'UTF-8');
        if ($this->input->post('primer_apellido_agresor') !='') $caso['primer_apellido_agresor'] = mb_strtoupper($this->input->post('primer_apellido_agresor'), 'UTF-8');
        if ($this->input->post('segundo_apellido_agresor') !='') $caso['segundo_apellido_agresor'] = mb_strtoupper($this->input->post('segundo_apellido_agresor'), 'UTF-8');

        if ($this->input->post('primer_nombre_victima') !='') $caso['primer_nombre_victima'] = mb_strtoupper($this->input->post('primer_nombre_victima'), 'UTF-8');
        if ($this->input->post('segundo_nombre_victima') !='') $caso['segundo_nombre_victima'] = mb_strtoupper($this->input->post('segundo_nombre_victima'), 'UTF-8');
        if ($this->input->post('primer_apellido_victima') !='') $caso['primer_apellido_victima'] = mb_strtoupper($this->input->post('primer_apellido_victima'), 'UTF-8');
        if ($this->input->post('segundo_apellido_victima') !='') $caso['segundo_apellido_victima'] = mb_strtoupper($this->input->post('segundo_apellido_victima'), 'UTF-8');

        if (isset($datos['otra_entidad'])) {
            $datos['entidad'] = $datos['otra_entidad'];
            unset($datos['otra_entidad']);
        }

        $datos['profesional'] = $this->session->userdata('usuario');

        //INSERTAR DATOS
        $this->db->insert('llamadas', $datos);
        $id = $this->db->insert_id();
        if (isset($caso)) {
            $caso['id_llamada_vio'] = $id;
            $this->db->insert('violencias', $caso);
        }
        $data['mensaje'] = 'DATOS GUARDADOS EXITOSAMENTE';
        $this->load->view('exito', $data);
        $this->load->view('pie');

    }

    //BUSCA LLAMADAS ANTERIORES O POR UN TELEFONO DADO
    function anteriores()
    {
        //RECUPERAR REGISTROS
        $data['filas'] = 50;
        $data['desde'] = (int)$this->uri->segment(3);

		//BUSCAR ID LLAMADA
        if ($this->input->post('llamada_id') !="") {
          $data['llamada_id'] = $this->input->post('llamada_id');
          $this->session->set_userdata('llamada_id', $this->input->post('llamada_id'));
        } elseif ($this->session->llamada_id !="" ) {
            $data['llamada_id'] = $this->session->llamada_id;
        } else {
            $data['llamada_id'] = "";
        }
				
        //BUSCAR NOMBRE
        if ($this->input->post('palabra') !="") {
            $data['palabra'] = $this->input->post('palabra');
            $this->session->set_userdata('palabra', $this->input->post('palabra'));
        } elseif ($this->session->userdata('palabra') !="" ) {
            $data['palabra'] = $this->session->userdata('palabra');
        } else {
            $data['palabra'] = "";
        }
        //BUSCAR TELEFONO
        if ($this->input->post('telefono') !="") {
            $data['telefono'] = $this->input->post('telefono');
            $this->session->set_userdata('telefono', $this->input->post('telefono'));
        } elseif ($this->session->userdata('telefono') !="" ) {
            $data['telefono'] = $this->session->userdata('telefono');
        } else {
            $data['telefono'] = "";
        }
        //BUSCAR EDAD
        if ($this->input->post('rango') !="") {
            $data['rango'] = $this->input->post('rango');
            $this->session->set_userdata('rango', $this->input->post('rango'));
        } elseif ($this->session->userdata('rango') !="" ) {
            $data['rango'] = $this->session->userdata('rango');
        } else {
            $data['rango'] = "";
        }
        //BUSCAR INTERVENCION
        if ($this->input->post('linea_id') !="") {
            $data['linea_id'] = $this->input->post('linea_id');
            $this->session->set_userdata('linea_id', $this->input->post('linea_id'));
        } elseif ($this->session->userdata('linea_id') !="" ) {
            $data['linea_id'] = $this->session->userdata('linea_id');
        } else {
            $data['linea_id'] = "";
        }
        //BUSCAR ENTIDAD
        if ($this->input->post('entidad') !="") {
            $data['entidad'] = $this->input->post('entidad');
            $this->session->set_userdata('entidad', $this->input->post('entidad'));
        } elseif ($this->session->userdata('entidad') !="" ) {
            $data['entidad'] = $this->session->userdata('entidad');
        } else {
            $data['entidad'] = "";
        }
        //BUSCAR INICIO Y FINAL
        $data['inicio'] = date("Y-m-d", mktime(0, 0, 0, date("m")-3 , 1, date("Y")));
        $data['final'] =  date("Y-m-d");
        if ($this->input->post('inicio') != "" or $this->input->post('final') != "") {
            $data['inicio'] = $this->input->post('inicio');
            $data['final'] = $this->input->post('final');
            $this->session->set_userdata('inicio', $this->input->post('inicio'));
            $this->session->set_userdata('final', $this->input->post('final'));
        } elseif ($this->session->userdata('inicio') !="" or $this->session->userdata('final')) {
            $data['inicio'] = $this->session->userdata('inicio');
            $data['final'] = $this->session->userdata('final');
        }
        //BUSCAR PROFESIONAL
        if ($this->input->post('profesional') !="") {
            $data['profesional'] = $this->input->post('profesional');
            $this->session->set_userdata('profesional', $this->input->post('profesional'));
        } elseif ($this->session->profesional !="" ) {
            $data['profesional'] = $this->session->profesional;
        } else {
            $data['profesional'] = "";
        }
        //LIMPIAR FORMULARIO
        if ($this->input->post('ok') == 'Limpiar' ) {
		    $data['llamada_id'] = "";
            $data['palabra'] = "";
            $data['telefono'] = "";
            $data['rango'] = "";
            $data['linea_id'] = "";
            $data['entidad'] = "";
            $data['profesional'] = "";
            $data['inicio'] = date("Y-m-d", mktime(0, 0, 0, date("m")-3 , 1, date("Y")));
            $data['final'] =  date("Y-m-d");
			$this->session->unset_userdata('llamada_id');
            $this->session->unset_userdata('palabra');
            $this->session->unset_userdata('telefono');
            $this->session->unset_userdata('rango');
            $this->session->unset_userdata('linea_id');
            $this->session->unset_userdata('inicio');
            $this->session->unset_userdata('final');
            $this->session->unset_userdata('entidad');
            $this->session->unset_userdata('profesional');
        }
        
        //CONSULTAS
        if ($data['palabra'] != "") {
            $data['total'] = $this->Busquedas->filas($data['palabra']);
            $data['llamadas'] = $this->Busquedas->llamadas($data['palabra'], $data['filas'], $data['desde']);
             
        } else { 

            $this->db->join('motivos', 'motivo_id = motivo_id_llam');
            $this->db->join('lineas', 'linea_id = linea_id_llam');
            $this->db->join('localidades', 'localidad_id = localidad_id_llam', 'left');
            $this->db->join('violencias', 'llamada_id = id_llamada_vio', 'left');
            $this->db->join('lugares', 'id_lugar = id_lugar_ocurre', 'left');
            $this->db->join('relaciones', 'id_relacion = id_relacion_agresor_victima', 'left');
            $this->db->join('edades', 'edades.edad = llamadas.edad', 'left');
            $this->db->order_by("llamada_id", "desc");
            $this->db->where('hora_inicio >', $data['inicio'].' 00:00:00');
            $this->db->where('hora_inicio <', $data['final'].' 23:59:59');
            //Criterios
            if ($data['llamada_id'] != "") $this->db->where('llamada_id', $data['llamada_id']);
			if ($data['telefono'] != "") $this->db->like('telefono', $data['telefono']);
            if ($data['rango'] != "") $this->db->like('rango', $data['rango']);
            if ($data['linea_id'] != "") $this->db->like('linea_id_llam', $data['linea_id']);
            if ($data['entidad'] != "") $this->db->like('entidad', $data['entidad']);
            if ($data['profesional'] != "") $this->db->like('profesional', $data['profesional']);
            //Get
            $data['llamadas'] = $this->db->get('llamadas', $data['filas'], $data['desde']);
            //Total
            $this->db->select('COUNT(*) AS total');
            $this->db->join('edades', 'edades.edad = llamadas.edad', 'left');
            $this->db->where('hora_inicio >', $data['inicio'].' 00:00:00');
            $this->db->where('hora_inicio <', $data['final'].' 23:59:59');
            if ($data['telefono'] != "") $this->db->like('telefono', $data['telefono']);
            if ($data['telefono'] != "") $this->db->like('telefono', $data['telefono']);
            if ($data['rango'] != "") $this->db->like('rango', $data['rango']);
            if ($data['linea_id'] != "") $this->db->like('linea_id_llam', $data['linea_id']);
            if ($data['entidad'] != "") $this->db->like('entidad', $data['entidad']);
            $query = $this->db->get('llamadas');
            $data['total'] = $query->first_row()->total;
        }

        //CONFIGURAR PAGINADOR
        $config['base_url'] = site_url('/llamadas/anteriores/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $data['filas'];
        $config['num_links'] = '5';
        $config['first_link'] = 'primero';
        $config['last_link'] = 'último';
        $config['full_tag_open'] = '<div style="text-align: center;">';
        $config['full_tag_close'] = '</div>';

        $this->pagination->initialize($config);
        
        //Combo Edades
        $data['edades'][''] = '-';
        $this->db->group_by('rango');
        $query = $this->db->get('edades');
        foreach ($query->result() as $row) {
            $data['edades'][$row->rango] = $row->rango;
        }
        //Combo Lineas
        $data['lineas'][''] = '-';
        $query = $this->db->get('lineas');
        foreach ($query->result() as $row) {
            $data['lineas'][$row->linea_id] = $row->linea;
        }
        //Combo Entidades
        $data['entidades'][''] = "-";
        $this->db->order_by('tipo_entidad');
        $query = $this->db->get("entidades");
        foreach ($query->result() as $row) {
            $data['entidades'][$row->entidad] = $row->tipo_entidad.': '.$row->entidad;
        }
        //Combo Usuarios
        $data['usuarios'][''] = "- TODOS -";
        $this->db->where('clave IS NOT NULL');
        $this->db->order_by('nombre_usuario');
        $query = $this->db->get('usuarios');
        foreach ($query->result() as $row) {
            $data['usuarios'][$row->usuario] = $row->nombre_usuario.' '.$row->apellido_usuario;
        }
        //Cargar Formulario
        $this->load->view('anteriores', $data);
        $this->load->view('pie');
    }

    //EDITA LLAMADA ANTERIOR
    function editar()
    {
        if (isset($_POST['llamada_id'])) {

            //BUSCAR LA LLAMADA
            $this->db->join('localidades', 'localidad_id = localidad_id_llam', 'left');
            $this->db->join('comos', 'como_id = como_id_llam', 'left');
            $this->db->join('etnias', 'etnia_id = etnia_id_llam', 'left');
            $this->db->join('motivos', 'motivo_id = motivo_id_llam');
            $this->db->join('lineas', 'linea_id = linea_id_llam');
            $this->db->where('llamada_id', $_POST['llamada_id']);
            $query = $this->db->get('llamadas');

            $data['llamada'] = $query->first_row('array');

            $this->load->view('editar_llamada', $data);
            $this->load->view('pie');
        }
    }

    //GUARDA DATOS EDITADOS
    function guardar_editado()
    {
        $datos['hora_registro'] = date ("Y-m-d H:i:s");
        $datos['narrativa'] = $this->input->post('narrativa');
        $datos['intervencion'] = $this->input->post('intervencion');
        $datos['observaciones'] = $this->input->post('observaciones');

        $this->db->where('llamada_id', $this->input->post('llamada_id'));
        $this->db->update('llamadas', $datos);
        
        $data = array('mensaje' => 'LLAMADA EDITADA CON ÉXITO');

        $this->load->view('exito',$data);
        $this->load->view('pie');
    }

}

/* EOF */