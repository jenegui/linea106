<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ingreso extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Accesos');        
    }

    // BORRARA TODOS LOS DATOS SE SESSION Y LLAMAR EL LOGIN
    function index()
    {
        $this->session->sess_destroy();
        redirect('ingreso/login');
    }

    // PANTALLA DEL LOGIN
    function login()
    {
        if ($this->session->userdata('usuario' !="")) {
            redirect('ingreso');
        } else {
            $this->load->view('autenticar/encabezado');
            $this->load->view('autenticar/login');
        }
    }

    // VALIDAR USUARIO
    function validar()
    {
        $clave = md5($_POST['passwd']);
        //echo $clave; exit;
        $this->db->select("id_usuario, usuario, nombre_usuario, apellido_usuario, tipo_usuario, total_accesos");
        $this->db->where("usuario", $_POST['user']);
        $this->db->where("clave", $clave);

        $queryPass = $this->db->get("usuarios");
       
        if($queryPass->num_rows() == 0 ) {
            //ERROR DE LOGIN
            $newdata = array(   'usuario' => '',
                                'password' => '',
                                'tipo_usuario' => '');
            $this->session->set_userdata($newdata);
            redirect('ingreso/error');
        } else {
            //DATOS PARA LA SESION
            $newdata = $queryPass->first_row('array');
            $this->session->set_userdata($newdata);

            //REGISTRA EL ACCESO
            date_default_timezone_set('America/Bogota');
            $data = array(
               'total_accesos' => $newdata['total_accesos'] +1 ,
               'ultimo_acceso' => date("Y-m-d H:i:s") ,

            );
            $this->db->where('usuario', $newdata['usuario']);
            $this->db->update('usuarios', $data);
            
            $acceso = array(
                'usuario_acceso' => $newdata['usuario'],
                'hora_acceso' => date("Y-m-d H:i:s") 
            );
            $this->db->insert('accesos', $acceso);
             $str = $this->db->last_query();

            //OBLIGA A ACTUALIZAR EL PASSWORD LA PRIMERA VEZ O PASA A LA SIGUIENTE PANTALLA SEGUN EL TIPO DE USUARIO
            if ($_POST['passwd'] == '1234') {
                redirect('ingreso/actualizar/');
            } elseif ($this->session->userdata('tipo_usuario') == 'profesional') {
                redirect('llamadas/');
            } elseif ($this->session->userdata('tipo_usuario') == 'administrador') {
                redirect('llamadas/');
            } else {
                redirect('ingreso/no_perfil/');
            }

        }

    }

    // INICIA EL CAMBIO DE CLAVE Y DATOS DEL USUARIO
    function actualizar()
    {
        $this->db->where('usuario', $this->session->userdata('usuario'));
        $query = $this->db->get('usuarios') ;

        if($query->num_rows() == 0 ) {
            $this->load->view('autenticar/error_ingreso');
        } else {
            $data = $query->row_array();
            $data['mensaje'] = '';
            $this->load->view('autenticar/encabezado');
            $this->load->view('autenticar/cambio_password', $data);
            $this->load->view('pie');
        }
    }

    //REGISTRA EL CAMBIO DE CLAVE Y DATOS
    function cambiar()
    {
        // RECUPERA LOS DATOS Y CONVIERTE EL PASSWORD
        $this->db->where('usuario', $this->session->userdata('usuario'));
        $query = $this->db->get('usuarios') ;
        $row = $query->first_row('array');
        $row['oldpass'] = md5($this->input->post('oldpass'));

        if($query->num_rows() == 0 ) {
            $this->load->view('autenticar/error_ingreso');
        } else {
            // VALIDACIONES
            $data['mensaje'] = "";
            if ($row['oldpass'] != $row['clave']) {
                $data['mensaje'] .= "ERROR: LA CLAVE ANTERIOR NO COINCIDE !<br/>";
            }
            if ($this->input->post('newpass1') != $this->input->post('newpass2')) {
                $data['mensaje'] .= "ERROR: LA CONFIRMACION DE LA NUEVA CLAVE ES DIFERENTE !<br/>";
            }
            if (strlen($this->input->post('newpass1')) < 6 ) {
                $data['mensaje'] .= "ERROR: LA NUEVA CLAVE ES DEMASIADO CORTA !<br/>";
            }

            // GUARDAR CAMBIOS SI NO HAY ERROR
            if ($data['mensaje'] != "") {
                $data += $query->row_array();
                $this->load->view('autenticar/encabezado');
            } else {
                $update = array("nombre_usuario" => mb_strtoupper($this->input->post('nombres'), 'UTF-8'),
                                "apellido_usuario" => mb_strtoupper($this->input->post('apellidos'), 'UTF-8'),
                                "correo" => strtolower($this->input->post('correo')),
                                "ultimo_cambio" => date("Y-m-d H:i:s"),
                                "clave" => md5($this->input->post('newpass1')) );
                $this->db->where("usuario",$this->session->userdata('usuario'));
                $this->db->update("usuarios",$update);

                $data['mensaje'] = "ok";
            }
            $this->load->view('autenticar/cambio_password', $data);
            $this->load->view('pie');
        }
    }

   function no_perfil()
    {
        $this->load->view('autenticar/encabezado');
        $this->load->view('autenticar/no_perfil');
        $this->load->view('pie');
    }

    function error()
    {
        $this->load->view('autenticar/error_ingreso');
    }

    //Nuevo usuario de hospital
    function nuevo()
    {
        //Solo SDS puede crear usuarios
        $this->autenticar->tipo_usuario('administrador');
        $data['mensaje'] ="";
        unset($datos);

        //Registra el usuario
        if (@$_POST['ok'] == "Aceptar") {
            $datos = $_POST;
            $datos['usuario'] = strtolower($datos['usuario']);

            //Validaciones
            $this->db->where('usuario', $datos['usuario']);
            $query = $this->db->get('usuarios');
            if ($query->num_rows() >0)
                $data['mensaje'] .= "ERROR: El usuario ya está registrado en la base de datos!<br/>\n";

            if ($datos['usuario'] =="")
                $data['mensaje'] .= "ERROR: Falta el usuario!<br/>\n";

            //Guarda si no hay error
            if ($data['mensaje'] =="") {
                unset($datos['ok']);
                $datos['clave'] = md5($this->config->item('default_user_password'));
                $this->db->insert('usuarios', $datos);
                $data['mensaje'] .= "Usuario ". $datos['usuario']." fue registrado con éxito.";
            }

        }

        //Recupara lista de usuarios
        $this->db->order_by('nombre_usuario, apellido_usuario');
        $data['usuarios'] = $this->db->get('usuarios');


        $this->load->view('autenticar/encabezado');
        $this->load->view('autenticar/menu');
        $this->load->view('autenticar/nuevo_usuario', $data);
        $this->load->view('pie');
    }

    function reiniciar()
    {
        $datos['clave'] = md5($this->config->item('default_user_password'));
        $datos['ultimo_cambio'] = NULL;
        $this->db->where('id_usuario', $this->input->post('id_usuario'));
        $this->db->update('usuarios', $datos);
        redirect('ingreso/nuevo');
    }

    function anular()
    {
        $datos['clave'] = NULL;
        $datos['ultimo_cambio'] = date ("Y-m-d H:i:s");
        $this->db->where('id_usuario', $this->input->post('id_usuario'));
        $this->db->update('usuarios', $datos);
        redirect('ingreso/nuevo');
    }
}
?>