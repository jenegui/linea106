<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Aplicativo movido a otro servidor</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="author" content="Juan Pablo Berdejo"/>
    <link href="/sispic/estilos.css" rel="stylesheet" type="text/css"/>

	<style type="text/css">

	#box {
		position:absolute;
		top:25%;
		right:25%;
		bottom:25%;
		left:25%;
		padding:25px;
		margin:25px;
		}

	</style>

</head>
<body>

<div id="box">
<table>
    <tr>
        <td>
            <h1>Debido al cambio de servidor, el aplicativo estará
                cerrado por una hora.</h1>
        </td>
    </tr>
</table>
</div>

</body>
</html>


