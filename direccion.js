function construir_direccion()
{
    direccion="";
    if (document.form_ind.d1.value!="-"){
        direccion+=document.form_ind.d1.value;
    } else {
        alert("Falta parte de la dirección!");
        return 0;
    }

    if (document.form_ind.d2.value!="" && !(isNaN(document.form_ind.d2.value))){
        direccion+=" "+document.form_ind.d2.value;
    } else if (document.form_ind.d1.value!="RURAL") {
        alert("La vía principal no es un número!");
        return 0;
    }

    if (document.form_ind.d3.value!="-")
        direccion+=" "+document.form_ind.d3.value;
    if (document.form_ind.d4.value!="-")
        direccion+=" "+document.form_ind.d4.value;
    if (document.form_ind.d5.value!="-")
        direccion+=" "+document.form_ind.d5.value;
    if (document.form_ind.d6.value!="-")
        direccion+=" "+document.form_ind.d6.value;

    if (document.form_ind.d7.value!="" && !(isNaN(document.form_ind.d7.value))){
        direccion+=" "+document.form_ind.d7.value;
    } else if (document.form_ind.d1.value!="RURAL") {
        alert("La primera parte de la placa no es un número!");
        return 0;
    }

    if (document.form_ind.d8.value!="-")
        direccion+=" "+document.form_ind.d8.value;
    if (document.form_ind.d9.value!="-")
        direccion+=" "+document.form_ind.d9.value;
    if (document.form_ind.d10.value!="-")
        direccion+=" "+document.form_ind.d10.value;

    if (document.form_ind.d11.value!="" && !(isNaN(document.form_ind.d11.value))){
        direccion+=" "+document.form_ind.d11.value;
    } else if (document.form_ind.d1.value!="RURAL") {
        alert("La segunda parte de la placa no es un número!");
        return 0;
    }

    if (document.form_ind.d12.value!="-")
        direccion+=" "+document.form_ind.d12.value;
    if (document.form_ind.d13.value!="")
        direccion+=" | "+document.form_ind.d13.value;

    if (confirm("La dirección es: \n"+direccion)){
        document.form_ind.direccion.value=direccion;
        document.form_ind.d1.disabled=true;
        document.form_ind.d2.disabled=true;
        document.form_ind.d3.disabled=true;
        document.form_ind.d4.disabled=true;
        document.form_ind.d5.disabled=true;
        document.form_ind.d6.disabled=true;
        document.form_ind.d7.disabled=true;
        document.form_ind.d8.disabled=true;
        document.form_ind.d9.disabled=true;
        document.form_ind.d10.disabled=true;
        document.form_ind.d11.disabled=true;
        document.form_ind.d12.disabled=true;
        document.form_ind.d13.disabled=true;
        document.form_ind.d14.disabled=true;
        document.form_ind.d15.disabled=false;
    }
}

function opciones_direccion()
{
    if(document.form_ind.d1.value=="KR" || document.form_ind.d1.value=="TV" || document.form_ind.d1.value=="AK"){
        document.form_ind.d6.length = 0;
        document.form_ind.d6.options[document.form_ind.d6.options.length] = new Option('-', '-');
        document.form_ind.d6.options[document.form_ind.d6.options.length] = new Option('ESTE', 'ESTE');
        document.form_ind.d12.length = 0;
        document.form_ind.d12.options[document.form_ind.d12.options.length] = new Option('-', '-');
        document.form_ind.d12.options[document.form_ind.d12.options.length] = new Option('SUR', 'SUR');
        document.form_ind.d2.disabled=false;
        document.form_ind.d3.disabled=false;
        document.form_ind.d4.disabled=false;
        document.form_ind.d5.disabled=false;
        document.form_ind.d6.disabled=false;
        document.form_ind.d7.disabled=false;
        document.form_ind.d8.disabled=false;
        document.form_ind.d9.disabled=false;
        document.form_ind.d10.disabled=false;
        document.form_ind.d11.disabled=false;
        document.form_ind.d12.disabled=false;
        document.form_ind.d13.disabled=false;
        document.form_ind.d14.disabled=false;

    } else if(document.form_ind.d1.value=="CL" || document.form_ind.d1.value=="DG" || document.form_ind.d1.value=="AC"){
        document.form_ind.d6.length = 0;
        document.form_ind.d6.options[document.form_ind.d6.options.length] = new Option('-', '-');
        document.form_ind.d6.options[document.form_ind.d6.options.length] = new Option('SUR', 'SUR');
        document.form_ind.d12.length = 0;
        document.form_ind.d12.options[document.form_ind.d12.options.length] = new Option('-', '-');
        document.form_ind.d12.options[document.form_ind.d12.options.length] = new Option('ESTE', 'ESTE');
        document.form_ind.d2.disabled=false;
        document.form_ind.d3.disabled=false;
        document.form_ind.d4.disabled=false;
        document.form_ind.d5.disabled=false;
        document.form_ind.d6.disabled=false;
        document.form_ind.d7.disabled=false;
        document.form_ind.d8.disabled=false;
        document.form_ind.d9.disabled=false;
        document.form_ind.d10.disabled=false;
        document.form_ind.d11.disabled=false;
        document.form_ind.d12.disabled=false;
        document.form_ind.d13.disabled=false;
        document.form_ind.d14.disabled=false;

    } else if(document.form_ind.d1.value=="RURAL"){
        document.form_ind.d2.value="";
        document.form_ind.d3.value="-";
        document.form_ind.d4.value="-";
        document.form_ind.d5.value="-";
        document.form_ind.d6.value="-";
        document.form_ind.d7.value="";
        document.form_ind.d8.value="-";
        document.form_ind.d9.value="-";
        document.form_ind.d10.value="-";
        document.form_ind.d11.value="";
        document.form_ind.d12.value="-";
        document.form_ind.d2.disabled=true;
        document.form_ind.d3.disabled=true;
        document.form_ind.d4.disabled=true;
        document.form_ind.d5.disabled=true;
        document.form_ind.d6.disabled=true;
        document.form_ind.d7.disabled=true;
        document.form_ind.d8.disabled=true;
        document.form_ind.d9.disabled=true;
        document.form_ind.d10.disabled=true;
        document.form_ind.d11.disabled=true;
        document.form_ind.d12.disabled=true;
        document.form_ind.d13.disabled=false;
        document.form_ind.d14.disabled=false;
    } else {
        document.form_ind.d2.disabled=true;
        document.form_ind.d3.disabled=true;
        document.form_ind.d4.disabled=true;
        document.form_ind.d5.disabled=true;
        document.form_ind.d6.disabled=true;
        document.form_ind.d7.disabled=true;
        document.form_ind.d8.disabled=true;
        document.form_ind.d9.disabled=true;
        document.form_ind.d10.disabled=true;
        document.form_ind.d11.disabled=true;
        document.form_ind.d12.disabled=true;
        document.form_ind.d13.disabled=true;
        document.form_ind.d14.disabled=true;
    }
}

function activar_direccion()
{
    document.form_ind.d1.disabled=false;
    document.form_ind.d1.selectedIndex=0;
    document.form_ind.d15.disabled=true;
}