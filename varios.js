function stayHere() {
    window.history.forward();
}

function solo_edad()
{
    var answer = confirm("Si sólo tiene el dato de la edad, el sistema calculará una fecha de nacimiento aproximada")
    if (answer){
        document.form_ind.edad.style.visibility="visible";
        document.form_ind.btnEdad.style.visibility="hidden";
        document.form_ind.txtFechaNacimiento.readOnly=true;
        document.form_ind.txtFechaNacimiento.value='Seleccione la edad';
    }
}

function calcular_fecha()
{
    var d = new Date();
    var year = d.getFullYear()-document.form_ind.edad.value;
    var month = ""+(d.getMonth()+1), day = ""+d.getDate();
    if(month.length==1) month = "0"+month;
    if(day.length==1) day = "0"+day;
    var fecha = (year + "-" + month + "-" + day)
    document.form_ind.txtFechaNacimiento.value=fecha;
}

function opciones_poblacion()
{
    if (document.form_ind.ninguna.checked) {
        document.form_ind.desplazado.checked=false;
        document.form_ind.discapacidad.checked=false;
        document.form_ind.habitante_calle.checked=false;
        document.form_ind.cabeza_familia.checked=false;
    }
}

function validarEntero(valor){
    //intento convertir a entero.
    //si era un entero no le afecta, si no lo era lo intenta convertir
    valor = parseInt(valor)

    //Compruebo si es un valor numérico
    if (isNaN(valor)) {
        //entonces (no es numero) devuelvo el valor 0
        return 0
      } else {
        //En caso contrario (Si era un número) devuelvo el valor
        return valor
      }
}

function popup(mylink, windowname)
{
    if (! window.focus)return true;
    var href;
    if (typeof(mylink) == 'string')
       href=mylink;
    else
       href=mylink.href;
    window.open(href, windowname, 'width=500,height=400,left=50,top=50,scrollbars=yes,resizable=yes');
    return false;
}


// Funciones de validación de fechas, adaptado de
// http://informatica-practica.net/solocodigo/index.php/2007/12/10/validar-formato-de-fecha-con-javascript/

  function esFechaValida(fecha) {

      if (fecha != undefined && fecha.value != "" ) {

          if (!/^\d{4}-\d{2}-\d{2}$/.test(fecha.value)) {

              //alert("formato de fecha no válido (aaaa-mm-dd)");
              return false;
          }

          var anio =  parseInt(fecha.value.substring(0, 4), 10);
          var mes  =  parseInt(fecha.value.substring(5, 7), 10);
          var dia  =  parseInt(fecha.value.substring(8), 10);

          switch(mes) {

              case 1:
              case 3:
              case 5:
              case 7:
              case 8:
              case 10:
              case 12:
                  numDias = 31;
                  break;

              case 4: case 6: case 9: case 11:
                  numDias = 30;
                  break;

              case 2:
                  if (comprobarSiBisisesto(anio)) { numDias = 29 } else { numDias = 28 };
                  break;

              default:
                  //alert("Fecha introducida errónea");
                  return false;
          }

          if (dia > numDias || dia == 0) {

              //alert("Fecha introducida errónea");
              return false;
          }

          return true;
      }

      return false;
  }


  function comprobarSiBisisesto(anio) {

      if ( ( anio % 100 != 0) && ((anio % 4 == 0) || (anio % 400 == 0)))
          return true;
      else
          return false;
  }

